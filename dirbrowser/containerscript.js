// This script saves the location details when a mouse button is
// clicked. This is for replacing data provided by QWebElement on a
// right-click when using WebEngine which does not have
// an equivalent service. 
var locDetails = '';
function saveLoc(ev) 
{
    el = ev.target;
    locDetails = '';
    while (el && el.attributes && !el.attributes.getNamedItem("objid")) {
        // console.log('el: ' + el);
        el = el.parentNode;
    }
    
    objid = el.attributes.getNamedItem("objid");
    if (objid) {
        objidvalue = objid.value;
    } else {
        objidvalue = "";
    }
 
    objidx = el.attributes.getNamedItem("objidx");
    if (objidx) {
        objidxvalue = objidx.value;
    } else {
        objidxvalue = "";
    }
 
   otype = el.attributes.getNamedItem("class");
    if (otype) {
        otypevalue = otype.value
    } else {
        otypevalue = "";
    }

    ispl = el.attributes.getNamedItem("ispl");
    if (ispl) {
        isplvalue = ispl.value
    } else {
        isplvalue = "";
    }

    if (el && el.attributes) {
        locDetails = 
            'objid = ' + objidvalue + '\n' +
            'title = ' + (el.textContent || el.innerText || "") + '\n' +
            'objidx = ' + objidxvalue + '\n' +
            'ispl = ' + isplvalue + '\n' +
            'otype = ' + otypevalue + '\n';
    }
    //console.log(locDetails);
}

// From https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#Solution_2_%E2%80%93_rewrite_the_DOMs_atob()_and_btoa()_using_JavaScript's_TypedArrays_and_UTF-8
// Not sure where TextDecoder comes from. See
// https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder
// and https://github.com/coolaj86/TextEncoderLite ?
function Base64Decode(str, encoding = 'utf-8') {
    var bytes = toByteArray(str);
    return new (TextDecoder || TextDecoderLite)(encoding).decode(bytes);
}

// From https://github.com/beatgammit/base64-js/blob/master/index.js,
var lookup = []
var revLookup = []
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array

var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i]
  revLookup[code.charCodeAt(i)] = i
}

// Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications
revLookup['-'.charCodeAt(0)] = 62
revLookup['_'.charCodeAt(0)] = 63

function getLens (b64) {
  var len = b64.length

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4')
  }

  // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42
  var validLen = b64.indexOf('=')
  if (validLen === -1) validLen = len

  var placeHoldersLen = validLen === len
    ? 0
    : 4 - (validLen % 4)

  return [validLen, placeHoldersLen]
}

// base64 is 4/3 + up to two characters of the original data
function byteLength (b64) {
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function _byteLength (b64, validLen, placeHoldersLen) {
  return ((validLen + placeHoldersLen) * 3 / 4) - placeHoldersLen
}

function toByteArray (b64) {
  var tmp
  var lens = getLens(b64)
  var validLen = lens[0]
  var placeHoldersLen = lens[1]

  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen))

  var curByte = 0

  // if there are placeholders, only get up to the last complete 4 chars
  var len = placeHoldersLen > 0
    ? validLen - 4
    : validLen

  for (var i = 0; i < len; i += 4) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 18) |
      (revLookup[b64.charCodeAt(i + 1)] << 12) |
      (revLookup[b64.charCodeAt(i + 2)] << 6) |
      revLookup[b64.charCodeAt(i + 3)]
    arr[curByte++] = (tmp >> 16) & 0xFF
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 2) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 2) |
      (revLookup[b64.charCodeAt(i + 1)] >> 4)
    arr[curByte++] = tmp & 0xFF
  }

  if (placeHoldersLen === 1) {
    tmp =
      (revLookup[b64.charCodeAt(i)] << 10) |
      (revLookup[b64.charCodeAt(i + 1)] << 4) |
      (revLookup[b64.charCodeAt(i + 2)] >> 2)
    arr[curByte++] = (tmp >> 8) & 0xFF
    arr[curByte++] = tmp & 0xFF
  }

  return arr
}

