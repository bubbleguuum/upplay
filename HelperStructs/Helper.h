/* Copyright (C) 2011  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _HELPER_H
#define _HELPER_H
#include <time.h>

#include <string>
#include <vector>

#include <QString>
#include <QList>
#include <QWidget>
#include <QByteArray>

namespace Helper {
QByteArray readFileToByteArray(const QString& fn);
bool read_file_into_str(QString filename, QString* content);

QString cvtMsecs2TitleLengthString(long int msec, bool colon = true,
                                   bool show_days = true);
void setStyleSubDir(const QString& subd);
QString getIconDir();
QString getIconPath(const QString& nm);
QString getSharePath();
QString getCSSPath();
QString getHomeDataPath();
QString createLink(QString name, QString target = "", bool underline = true);
// Escape things that would look like HTML markup
std::string escapeHtml(const std::string &in);
QString escapeHtml(const QString& in);
extern void msleep(int millis);
}

extern std::string ivtos(const std::vector<int>& nids);

#endif
