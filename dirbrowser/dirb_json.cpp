/* Copyright (C) 2014 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "dirbrowser.h"

#include <string>
#include <sstream>
#include <memory>
#include <iostream>

#include <json/json.h>

using namespace std;

string DirBrowser::stateToJson()
{
    Json::Value root;

    for (int i = 0; i < ui->tabs->count(); i++) {
        CDBrowser *cdb = cdbFromTabIndex(i);
        if (nullptr == cdb) {
            return string();
        }
        Json::Value tab;
        string udn = cdb->getServerUDN();
        if (udn.empty()) {
            continue;
        }

        tab["UDN"] = cdb->getServerUDN();
        Json::Value path;
        for (const auto& pe : cdb->getcurpath()) {
            Json::Value jspe;
            jspe["objid"] = pe.objid;
            jspe["title"] = pe.title;
            jspe["isPlaylist"] = pe.isPlaylist;
            jspe["searchStr"] = pe.searchStr;
            jspe["scrollposX"] = pe.scrollpos.x();
            jspe["scrollposY"] = pe.scrollpos.y();
            // The scrollpos is only set when browsing a subdir. So
            // get it directly for the last element, because it's not
            // set there.
            if (&pe == &cdb->getcurpath().back()) {
                QPoint sp = cdb->scrollpos();
                jspe["scrollposX"] = sp.x();
                jspe["scrollposY"] = sp.y();
            }
            path.append(jspe);
        }
        tab["path"] = path;

        root.append(tab);
    }

    // Was: Json::FastWriter fastWriter;string out = fastWriter.write(root);
    // But, deprecated, why keep things simple ?
    std::ostringstream ostr;
    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
    writer->write(root, &ostr);
    // std::cerr << "JSON: " << ostr.str() << "\n";
    return ostr.str();
}

vector<pair<string, vector<CDBrowser::CtPathElt>>>
DirBrowser::stateFromJson(const string& jsonstr)
{
    vector<pair<string, vector<CDBrowser::CtPathElt>>> ret;

    Json::Value root;
    Json::CharReaderBuilder builder;
    std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
    std::string errs;
    const char *cp = jsonstr.c_str();
    bool ok = reader->parse(cp, cp + jsonstr.size(), &root, &errs);
    if (!ok) {
        std::cerr << "DirBrowser::stateFromJson: could not parse json string: "
                  << errs << "\n";
        return ret;
    }
    for (Json::ArrayIndex tabidx = 0; tabidx < root.size(); tabidx++) {
        string udn = root[tabidx]["UDN"].asString();
        ret.push_back(pair<string, vector<CDBrowser::CtPathElt>>(
                          udn, vector<CDBrowser::CtPathElt>()));
        CDBrowser::CtPathElt pe;
        Json::Value& jspath = root[tabidx]["path"];
        for (Json::ArrayIndex pathidx = 0; pathidx < jspath.size(); pathidx++) {
            Json::Value& jspe = jspath[pathidx];
            pe.objid =        jspe["objid"].asString();
            pe.title =        jspe["title"].asString();
            pe.isPlaylist =   jspe["isPlaylist"].asBool();
            pe.searchStr =    jspe["searchStr"].asString();
            pe.scrollpos = QPoint(jspe["scrollposX"].asInt(),
                                  jspe["scrollposY"].asInt());
            ret.back().second.push_back(pe);
        }
    }
    return ret;
}
