TEMPLATE        = app
TARGET          = upplay

# VERSION is ^egrepped and must stay in the first column
VERSION = 1.4.6
COPYRDATES = 2011-2020

#WEBPLATFORM = webengine
contains(WEBPLATFORM, webengine) {
    QT += widgets webenginewidgets
    QMAKE_CXXFLAGS += -DUSING_WEBENGINE
    DEFINES += USING_WEBENGINE
} else {
    # Webkit (qt4 or 5)
    QT += webkit
    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webkitwidgets
}

QMAKE_CXXFLAGS += -DUPPLAY_VERSION=\\\"$$VERSION\\\" 
QMAKE_CXXFLAGS += -DUPPLAY_COPYRDATES=\\\"$$COPYRDATES\\\"
INCLUDEPATH += $$PWD/utils

CONFIG  += qt warn_on thread release

# DEFINES += UPPLAY_HORIZONTAL_LAYOUT

# This is only used for the icons which are referenced from the style sheets.
# Did not find any other reasonably simple way to get rid of it.
RESOURCES += GUI/upplay.qrc

HEADERS += \
        GUI/mainw/mainw.h \
        GUI/mainw/trayicon.h \
        GUI/mdatawidget/mdatawidget.h \
        GUI/mdatawidget/mdatawidgetif.h \
        GUI/playctlwidget/playctlwidget.h \
        GUI/playctlwidget/playctlwidgetif.h \
        GUI/playerwidget/playerhwidget.h \
        GUI/playerwidget/playervwidget.h \
        GUI/playlist/GUI_Playlist.h \
        GUI/playlist/delegate/PlaylistItemDelegate.h \
        GUI/playlist/entry/GUI_PlaylistEntry.h \
        GUI/playlist/entry/GUI_PlaylistEntryBig.h \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.h \
        GUI/playlist/model/PlaylistItemModel.h \
        GUI/playlist/view/ContextMenu.h \
        GUI/playlist/view/PlaylistView.h \
        GUI/prefs/prefs.h \
        GUI/prefs/sortprefs.h \
        GUI/progresswidget/progresswidget.h \
        GUI/progresswidget/progresswidgetif.h \
        GUI/renderchoose/renderchoose.h \
        GUI/songcast/songcastdlg.h \
        GUI/sourcechoose/sourcechoose.h \
        GUI/volumewidget/soundslider.h  \
        GUI/volumewidget/volumewidget.h \
        GUI/volumewidget/volumewidgetif.h \
        GUI/widgets/directslider.h  \
        HelperStructs/CSettingsStorage.h \
        HelperStructs/Helper.h \
        HelperStructs/PlaylistMode.h \
        HelperStructs/Style.h \
        application.h \
        dirbrowser/cdbrowser.h \
        dirbrowser/dirbrowser.h \
        dirbrowser/randplayer.h \
        dirbrowser/rreaper.h \
        notifications/audioscrobbler.h \
        notifications/notifications.h \
        playlist/playlist.h \
        playlist/playlistavt.h \
        playlist/playlistnull.h \
        playlist/playlistohpl.h \
        playlist/playlistohrcv.h \
        playlist/playlistohrd.h \
        upadapt/avtadapt.h \
        upadapt/ohifadapt.h \
        upadapt/ohpladapt.h \
        upadapt/ohrdadapt.h \
        upadapt/songcast.h \
        upqo/avtransport_qo.h \
        upqo/cdirectory_qo.h \
        upqo/ohinfo_qo.h \
        upqo/ohplaylist_qo.h \
        upqo/ohproduct_qo.h \
        upqo/ohradio_qo.h \
        upqo/ohreceiver_qo.h \
        upqo/ohtime_qo.h \
        upqo/ohvolume_qo.h \
        upqo/renderingcontrol_qo.h \
        utils/confgui.h \
        utils/smallut.h \
        utils/writedescription.h
                        
SOURCES += \
        GUI/mainw/mainw.cpp \
        GUI/mainw/mw_connections.cpp  \
        GUI/mainw/mw_controls.cpp  \
        GUI/mainw/mw_cover.cpp  \
        GUI/mainw/mw_menubar.cpp  \
        GUI/mainw/trayicon.cpp \
        GUI/mdatawidget/mdatawidget.cpp \
        GUI/playctlwidget/playctlwidget.cpp \
        GUI/playerwidget/playerwidget.cpp \
        GUI/playlist/GUI_Playlist.cpp \
        GUI/playlist/delegate/PlaylistItemDelegate.cpp \
        GUI/playlist/entry/GUI_PlaylistEntryBig.cpp \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.cpp \
        GUI/playlist/model/PlaylistItemModel.cpp \
        GUI/playlist/view/ContextMenu.cpp \
        GUI/playlist/view/PlaylistView.cpp \
        GUI/prefs/prefs.cpp \
        GUI/prefs/sortprefs.cpp \
        GUI/progresswidget/progresswidget.cpp \
        GUI/songcast/songcastdlg.cpp \
        GUI/volumewidget/soundslider.cpp  \
        GUI/volumewidget/volumewidget.cpp \
        GUI/widgets/directslider.cpp \
        HelperStructs/CSettingsStorage.cpp \
        HelperStructs/Helper.cpp \
        HelperStructs/Style.cpp \
        application.cpp \
        dirbrowser/cdb_html.cpp \
        dirbrowser/cdbrowser.cpp \
        dirbrowser/dirb_json.cpp \
        dirbrowser/dirbrowser.cpp \
        dirbrowser/randplayer.cpp \
        dirbrowser/rreaper.cpp \
        notifications/audioscrobbler.cpp \
        notifications/notifications.cpp \
        playlist/playlist.cpp \
        playlist/playlistavt.cpp \
        playlist/playlistohpl.cpp \
        playlist/playlistohrd.cpp \
        upadapt/rendererlist.cpp \
        upadapt/songcast.cpp \
        upadapt/upputils.cpp \
        upplay.cpp \
        upqo/ohpool.cpp \
        utils/confgui.cpp \
        utils/conftree.cpp \
        utils/md5.cpp \
        utils/pathut.cpp \
        utils/smallut.cpp \
        utils/utf8iter.cpp \
        utils/writedescription.cpp

# These are not used for now (local radio station list for avt
# players). Maybe one day.
#        playlist/playlistlocrd.h 
#        playlist/playlistlocrd.cpp 

FORMS   = \
        GUI/mainw/mainhw.ui \
        GUI/mainw/mainw.ui \
        GUI/playctlwidget/playctlwidget.ui \
        GUI/playerwidget/playerhwidget.ui \
        GUI/playerwidget/playervwidget.ui \
        GUI/playlist/GUI_Playlist.ui \
        GUI/playlist/entry/GUI_PlaylistEntryBig.ui \
        GUI/playlist/entry/GUI_PlaylistEntrySmall.ui \
        GUI/prefs/sortprefs.ui \
        GUI/progresswidget/progresswidget.ui \
        GUI/renderchoose/renderchoose.ui \
        GUI/songcast/songcastdlg.ui \
        GUI/sourcechoose/sourcechoose.ui

contains(WEBPLATFORM, webengine) {
    FORMS += dirbrowser/dirbrowser-webengine.ui
} else {
    FORMS += dirbrowser/dirbrowser-webkit.ui
}

unix:!mac {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  LIBS += -lupnpp -ljsoncpp
  isEmpty(PREFIX) {
    PREFIX = /usr
  }
  message("Prefix is $$PREFIX")
  DEFINES += PREFIX=\\\"$$PREFIX\\\"
  DEFINES += _LARGE_FILE_SOURCE
  DEFINES += _FILE_OFFSET_BITS=64

  INCLUDEPATH += /usr/include/jsoncpp
  
 # Installation stuff
  target.path = "$$PREFIX/bin"

  bdata.files = dirbrowser/cdbrowser.css dirbrowser/dark.css \
              dirbrowser/standard.css dirbrowser/containerscript.js
  bdata.path = $$PREFIX/share/upplay/cdbrowser   
  gdata.files = GUI/common.qss GUI/standard.qss GUI/dark.qss
  gdata.path = $$PREFIX/share/upplay/
  desktop.files += upplay.desktop
  desktop.path = $$PREFIX/share/applications/
  icona.files = GUI/icons/upplay.png
  icona.path = $$PREFIX/share/icons/hicolor/48x48/apps/
  iconb.files = GUI/icons/upplay.png
  iconb.path = $$PREFIX/share/pixmaps/
  iconc.files = GUI/icons/*.png
  iconc.path = $$PREFIX/share/upplay/icons
  icond.files = GUI/icons/*.png GUI/icons/dark/*.png
  icond.path = $$PREFIX/share/upplay/icons/dark

  INSTALLS += target bdata desktop gdata icona iconb iconc icond
}

mac {
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  LIBS += ../libupnpp/macos/build-libupnpp-Desktop_Qt_5_14_2_clang_64bit-Release/libupnpp.a
  LIBS += ../npupnp/macos/build-libnpupnp-Desktop_Qt_5_14_2_clang_64bit-Release/libnpupnp.a
  LIBS += ../libmicrohttpd-0.9.71/src/microhttpd/.libs/libmicrohttpd.a
  LIBS += -lexpat -lcurl

  INCLUDEPATH += sysdeps/jsoncpp
  INCLUDEPATH += ../libupnpp/
  DEFINES += _LARGE_FILE_SOURCE
  DEFINES += _FILE_OFFSET_BITS=64

  SOURCES += sysdeps/jsoncpp/jsoncpp.cpp

  ICON = macos/appIcon.icns
  APP_ICON_FILES.files = \
                         GUI/icons/addtab.png \
                         GUI/icons/append.png\
                         GUI/icons/arrow_down.png\
                         GUI/icons/arrow_down_orange.png\
                         GUI/icons/arrow_left.png\
                         GUI/icons/arrow_left_orange.png\
                         GUI/icons/arrow_right.png\
                         GUI/icons/arrow_right_orange.png\
                         GUI/icons/arrow_up.png\
                         GUI/icons/arrow_up_orange.png\
                         GUI/icons/broom.png\
                         GUI/icons/bwd.png\
                         GUI/icons/cb_checked.png\
                         GUI/icons/cb_checked_disabled.png\
                         GUI/icons/close.png\
                         GUI/icons/cross.png\
                         GUI/icons/dark\
                         GUI/icons/delete.png\
                         GUI/icons/dynamic.png\
                         GUI/icons/fwd.png\
                         GUI/icons/items.svg\
                         GUI/icons/logo.png\
                         GUI/icons/logo.xpm\
                         GUI/icons/numbers.png\
                         GUI/icons/pause.png\
                         GUI/icons/play.png\
                         GUI/icons/rb_checked.png\
                         GUI/icons/rb_checked_disabled.png\
                         GUI/icons/repAll.png\
                         GUI/icons/scroll_hor_bg.png\
                         GUI/icons/scroll_ver_bg.png\
                         GUI/icons/shuffle.png\
                         GUI/icons/slider_hor_bg.png\
                         GUI/icons/slider_ver_bg.png\
                         GUI/icons/stop.png\
                         GUI/icons/upplay.ico\
                         GUI/icons/upplay.png\
                         GUI/icons/vol_1.png\
                         GUI/icons/vol_2.png\
                         GUI/icons/vol_3.png\
                         GUI/icons/vol_mute.png\
                         GUI/icons/volume-slider-inside.png\
                         GUI/icons/volume-slider-outside.png
  APP_ICON_FILES.path = Contents/Resources

  APP_CSS_FILES.files = \
                         dirbrowser/cdbrowser.css \
                         dirbrowser/dark.css \
                         dirbrowser/standard.css
  APP_CSS_FILES.path = Contents/Resources

  APP_QSS_FILES.files = \
                         GUI/common.qss \
                         GUI/dark.qss \
                         GUI/standard.qss
  APP_QSS_FILES.path = Contents/Resources

  APP_JS_FILES.files = dirbrowser/containerscript.js
  APP_JS_FILES.path = Contents/Resources

  QMAKE_BUNDLE_DATA += APP_ICON_FILES APP_CSS_FILES APP_QSS_FILES APP_JS_FILES

  QMAKE_INFO_PLIST = macos/Info.plist

  # Installation stuff
  target.path = "/Applications"

  INSTALLS += target  
}

windows {
  # Using static libs: will get the one from npupnp
  SOURCES -= utils/utf8iter.cpp
  SOURCES += sysdeps/jsoncpp/jsoncpp.cpp
  INCLUDEPATH += sysdeps/jsoncpp
  INCLUDEPATH += c:/users/bill/documents/upnp/libupnpp

  DEFINES += PSAPI_VERSION=1

  contains(QMAKE_CC, gcc){
    # MingW
    QMAKE_CXXFLAGS += -std=c++11 -Wno-unused-parameter
    LIBS += c:/users/bill/documents/upnp/expat-2.1.0/.libs/libexpat.a
    LIBS += c:/users/bill/documents/upnp/curl-7.70.0/lib/libcurl.a
    LIBS += $$PWD/../libmicrohttpd-0.9.65/src/microhttpd/.libs/libmicrohttpd.a
    LIBS += $$PWD/../libupnpp/windows/build-libupnpp-Desktop_Qt_5_8_0_MinGW_32bit-Debug/debug/libupnpp.a
    LIBS += $$PWD/../npupnp/windows/build-libnpupnp-Desktop_Qt_5_8_0_MinGW_32bit-Debug/debug/libnpupnp.a
  }

  contains(QMAKE_CC, cl){
    # Visual Studio
    LIBS += $$PWD/../libupnpp/windows/build-libupnpp-Desktop_Qt_5_14_1_MSVC2017_32bit-Release/release/upnpp.lib
    LIBS += $$PWD/../npupnp/windows/build-libnpupnp-Desktop_Qt_5_14_1_MSVC2017_32bit-Release/release/npupnp.lib
    LIBS += c:/users/bill/documents/upnp/expat-2.2.9/bin/libexpat.lib
    LIBS += \
      $$PWD/../curl-7.70.0/builds/libcurl-vc-x86-release-static-ipv6/lib/libcurl_a.lib
    LIBS += \
      $$PWD/../libmicrohttpd-0.9.65-w32-bin/x86/VS2017/Release-dll/libmicrohttpd-dll.lib
    # Needed by libcurl in this config. Might be possible to ditch with
    # other curl build options ? First is for international urls second
    # is  hash/crypto lib
    LIBS += -lNormaliz
    LIBS += -lAdvapi32
  }

  LIBS += -liphlpapi
  LIBS += -lwldap32
  LIBS += -lws2_32
  LIBS += -lshlwapi
  LIBS += -lShell32
  LIBS += -luuid
}

contains(QMAKE_CC, gcc){
 QMAKE_CXXFLAGS += -std=c++11 -g
}
