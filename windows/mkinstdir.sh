#!/bin/sh
fatal()
{
    echo $*
    exit 1
}
Usage()
{
    fatal mkinstdir.sh targetdir
}

test $# -eq 1 || Usage

DESTDIR=$1

test -d $DESTDIR || mkdir $DESTDIR || fatal cant create $DESTDIR

# Script to make an upplay install directory from locally compiled
# software.

################################
# Local values (to be adjusted)

# We have two set of values on a w10 with msvc (webengine) and a w7
# with an older qt and mingw (which can build with webkit)
MSVC=1

# Upplay src tree
UPP=c:/users/bill/documents/upnp/upplay

LIBUPNPP=c:/users/bill/documents/upnp/libupnpp

if test -z "$MSVC"; then
    # Where to copy the Qt Dlls from:
    QTBIN=C:/Qt/Qt5.8.0/5.8/mingw53_32/bin
    # Qt arch
    QTA=Desktop_Qt_5_8_0_MinGW_32bit
    GUIBIN=$UPP/../build-upplay-${QTA}-Debug/release/upplay.exe
else
    QTBIN=C:/Qt/5.14.1/msvc2017/bin
    QTA=Desktop_Qt_5_14_1_MSVC2017_32bit
    GUIBIN=$UPP/../build-upplay-${QTA}-Release/release/upplay.exe
    MHTDLL=$UPP/../libmicrohttpd-0.9.65-w32-bin/x86/VS2017/Release-dll/libmicrohttpd-dll.dll
    EXPATDLL=$UPP/../expat-2.2.9/bin/libexpat.dll
fi

PATH=$QTBIN:$PATH
export PATH

# Check that the versions in libupnpp are consistent
chklibupnppversions()
{
    LIBH=$LIBUPNPP/libupnpp/upnpplib.hxx
    CONFH=$LIBUPNPP/libupnpp/config.h
    MAJOR=`grep '#define LIBUPNPP_VERSION_MAJOR' $LIBH | awk '{print $3}'`
    MINOR=`grep '#define LIBUPNPP_VERSION_MINOR' $LIBH | awk '{print $3}'`
    REVISION=`grep '#define LIBUPNPP_VERSION_REVISION' $LIBH | awk '{print $3}'`
    LIBSTRING='"'${MAJOR}.${MINOR}.${REVISION}'"'
    CONFSTRING=`grep '#define PACKAGE_VERSION' $CONFH | awk '{print $3}'`
    echo LIBSTRING $LIBSTRING CONFSTRING $CONFSTRING
    if test "$CONFSTRING" != "$LIBSTRING" ; then
        fatal inconsistent versions in config.h and upnpplib.hxx
    fi
}

# checkcopy. 
chkcp()
{
    cp $@ || fatal cp $@ failed
}

copyupplay()
{
    chkcp $UPP/dirbrowser/cdbrowser.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/standard.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/dark.css $DESTDIR/share/cdbrowser
    chkcp $UPP/dirbrowser/containerScript.js $DESTDIR/share/cdbrowser
    cp -rp $UPP/GUI/icons $DESTDIR/share
    chkcp $UPP/GUI/standard.qss $DESTDIR/share
    chkcp $UPP/GUI/dark.qss $DESTDIR/share
    chkcp $UPP/GUI/common.qss $DESTDIR/share
    chkcp $UPP/GUI/icons/upplay.ico $DESTDIR
    chkcp -rp $UPP/GUI/icons $DESTDIR
    chkcp $GUIBIN $DESTDIR
}

copyqt()
{
    cd $DESTDIR
    $QTBIN/windeployqt upplay.exe
    if test -z "$MSVC" ; then
        # Apparently because the webkit part was grafted "by hand" on the
        # Qt set, we need to copy some dll explicitely
        addlibs="Qt5Core.dll Qt5Multimedia.dll \
          Qt5MultimediaWidgets.dll Qt5OpenGL.dll \
          Qt5Positioning.dll Qt5PrintSupport.dll Qt5Sensors.dll \
          Qt5Sql.dll icudt57.dll \
          icuin57.dll icuuc57.dll libQt5WebKit.dll \
          libQt5WebKitWidgets.dll \
          libxml2-2.dll libxslt-1.dll"
        for i in $addlibs;do
            chkcp $QTBIN/$i $DESTDIR
        done
        chkcp $QTBIN/libwinpthread-1.dll $DESTDIR
        chkcp $QTBIN/libstdc++-6.dll $DESTDIR
    fi
}

copymht()
{
    if test -n "$MSVC"; then
       chkcp $MHTDLL $DESTDIR
    fi
}
copyexpat()
{
    if test -n "$MSVC"; then
       chkcp $EXPATDLL $DESTDIR
    fi
}

test -d $DESTDIR/share/cdbrowser || mkdir -p $DESTDIR/share/cdbrowser || exit 1
chklibupnppversions
copyupplay
copyqt
copymht
copyexpat
