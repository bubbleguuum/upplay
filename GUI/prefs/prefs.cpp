/* Copyright (C) 2017-2019 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <string>
#include <set>

#include <QSettings>
#include <QDebug>
#include <QMessageBox>

#include <libupnpp/upnpputils.hxx>

#include "upadapt/upputils.h"
#include "prefs.h"
#include "utils/confgui.h"
#include "utils/conftree.h"
#include "utils/smallut.h"
#include "sortprefs.h"
#include "upadapt/rendererlist.h"

using namespace std;
using namespace confgui;
using namespace UPnPClient;


// renderer-specific parameters: all parameters from the
// renderer-specific panel. Their processing is special because the
// data for the different parameters can't be stored in the GUI (only
// place for one), so we need to implement temporary storage, to be
// flushed on 'accept'

// The set of renderer parameter names is used by conflink to decide what to do
static set<string> rdrspecificnames{"usepauseforunpause"};

// Storage for the renderer-specific parameters
static ConfSimple o_rdrconf;

class ConfLinkQS : public confgui::ConfLinkRep {
public:
    ConfLinkQS(const QString& nm)
        : m_nm(nm) {
        if (rdrspecificnames.find(qs2utf8s(nm)) != rdrspecificnames.end()) {
            m_rdrspecific = true;
        }
    }
    virtual ~ConfLinkQS() {}

    // Compute the key for a rdr specific param: rdr fname + param name.
    static string rdrkey(const QString& nm, const QString& _rdr = QString()) {
        return rendererSettingsKey(nm, _rdr.isEmpty()? o_currentRenderer : _rdr);
    }

    virtual bool set(const std::string& val) {
        if (m_rdrspecific) {
            if (o_currentRenderer.isEmpty()) {
                return true;
            }
            string key = rdrkey(m_nm);
            LOGDEB("Cache Setting " << key << " -> " << val << endl);
            o_rdrconf.set(key, val);
        } else {
            QSettings settings;
            LOGDEB("Setting " << qs2utf8s(m_nm) << " -> " << val << endl);
            settings.setValue(m_nm, u8s2qs(val));
        }
        return true;
    }

    virtual bool get(std::string& val) {
        if (m_rdrspecific) {
            if (o_currentRenderer.isEmpty()) {
                return true;
            }
            string key = rdrkey(m_nm);
            return o_rdrconf.get(key, val);
        } else {
            QSettings settings;
            bool ret = settings.contains(m_nm);
            if (ret) {
                val = qs2utf8s(settings.value(m_nm).toString());
            }
            LOGDEB1("Get " << qs2utf8s(m_nm) << " -> " << val << endl);
            return ret;
        }
    }

    static void setCurrentRenderer(const QString& rdr) {
        o_currentRenderer = rdr;
    }

private:
    static QString o_currentRenderer;
    const QString m_nm;
    bool m_rdrspecific{false};
};
QString ConfLinkQS::o_currentRenderer;

class MyConfLinkFactQS : public confgui::ConfLinkFact {
public:
    MyConfLinkFactQS() {}
    virtual ConfLink operator()(const QString& nm) {
        ConfLinkRep *lnk = new ConfLinkQS(nm);
        return ConfLink(lnk);
    }
};
static MyConfLinkFactQS lnkfact;

// Called on accept: flush the local config to qsettings permanent storage.
// Note: at the moment, we don't delete the rdr-specific params from
// the settings when a renderer is taken of the list of renderers with
// specific params. The data is still in the file, but unused. This is
// hopefully not a problem.
// The list of renderers itself is managed by confgui in the normal way.
void UPPrefs::flushRdrParams()
{
    auto keys = o_rdrconf.getNames("");
    QSettings settings;
    for (const auto& key : keys) {
        string value;
        o_rdrconf.get(key, value);
        LOGDEB("Flush setting: " << key << " -> " << value << endl);
        settings.setValue(u8s2qs(key), u8s2qs(value));
    }
}

// Load renderer-specific params from QSettings at init. We then work
// exclusively with the temp storage until accept is called, at which
// time we flush.
void UPPrefs::initRdrParams()
{
    QSettings settings;
    // Fetch the set of renderers with specific parameters.
    string srdrs;
    vector<string> rdrs;
    if (settings.contains("rdrsel")) {
        srdrs = qs2utf8s(settings.value("rdrsel").toString());
        stringToStrings(srdrs, rdrs);
    }

    // For each renderer and each special param, update local storage
    for (const auto& rdr: rdrs) {
        for (const auto& nm : rdrspecificnames) {
            string key = ConfLinkQS::rdrkey(u8s2qs(nm), u8s2qs(rdr));
            QString qkey = u8s2qs(key);
            if (settings.contains(qkey)) {
                o_rdrconf.set(key, qs2utf8s(settings.value(qkey).toString()));
            }
        }
    }
}

// This is called when the current renderer in the renderer-specific
// panel changes
void UPPrefs::onCurrentRendererChanged(const QString& name)
{
    LOGDEB("UPPrefs::onCurrentRendererChanged: " << qs2utf8s(name)<< endl);
    ConfLinkQS::setCurrentRenderer(name);
    for (auto& paramp : m_rdrparams) {
        paramp->loadValue();
    }
}

void UPPrefs::onShowPrefs()
{
    if (m_w == 0) {
        initRdrParams();
        m_w = new ConfTabsW(m_parent, "UPPlay Preferences", &lnkfact);
        connect(m_w, SIGNAL(sig_prefsChanged()), this, SLOT(flushRdrParams()));
        int idx = m_w->addPanel("Application");

        m_w->addParam(idx, ConfTabsW::CFPT_STR, "wholeuiscale",
                      "Display scale (e.g. 1.0)",
                      "Use this to scale the interface (font sizes mostly) "
                      " up or down. May help on HiDPI screens.");

        // Restore tabs on startup ?
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "norestoretabs",
                      "Do not restore browsing state on startup",
                      "Start with one default initial tab instead of trying "
                      "to create the tabs and directory <br>"
                      "positions which existed when upplay was last closed.");

        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "saveseekpos",
                      "Save seek position when switching tracks",
                      "When switching tracks by double-clicking in the <br>"
                      "playlist, start play either at the last pause position "
                      "for this track if the playlist is in repeat mode, or "
                      "at the last play position");

        // Close to tray ?
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "min2tray", "Close to tray",
                      "Minimize to tray instead of exiting when the main window "
                      "is closed");

        // Show notifications ?
        ConfParamW *wsn =
            m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "shownotifications",
                          "Show notifications",
                          "Show pop-up message in notification area when a "
                          "track starts playing");
        QSettings settings;
#ifndef _WIN32
        ConfParamW *wuncmd =
            m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "usenotificationcmd",
                          "Use notification command (else use Qt)",
                          "Choose whether to use the Qt notification mechanism "
                          "or to execute a desktop command");
        if (!settings.contains("notificationcmd")) {
            settings.setValue("notificationcmd",
                              "notify-send --expire-time=1000 --icon=upplay");
        }
        ConfParamW *wncmd =
            m_w->addParam(idx, ConfTabsW::CFPT_STR, "notificationcmd",
                          "Notification command",
                          "Command to execute to notify. The message argument "
                          " will be appended");
        m_w->enableLink(wsn, wuncmd);
        m_w->enableLink(wuncmd, wncmd);
#endif
        
        m_w->addParam(idx, ConfTabsW::CFPT_INT, "maxcoverpopupsize", 
                      "Max player cover art popup height.", "", 500, 5000);

        // Truncate artist information in directory listings?
        ConfParamW *b1 =
            m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "truncateartistindir", 
                          "Truncate artist information in track lists",
                          "Truncate very long artist info so that the table "
                          "does not become weird");

        QString pname("truncateartistlen");
        if (!settings.contains(pname)) {
            settings.setValue(pname, 30);
        }
        ConfParamW *w1 =
            m_w->addParam(idx, ConfTabsW::CFPT_INT, pname, 
                          "Max artist information size in track lists.",
                          "", 0, 200);
        m_w->enableLink(b1, w1);


        // Show some artist information with albums ?
        b1 = m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "showartwithalb", 
                           "Show some artist information in album lists",
                           "List the beginning of artist info when listing "
                           "albums.\nThe exact amount can be adjusted with the "
                           "following entry", 1);
        pname = "artwithalblen";
        if (!settings.contains(pname)) {
            settings.setValue(pname, 15);
        }
        w1 = m_w->addParam(idx, ConfTabsW::CFPT_INT, pname, 
                           "Max artist info size in album lists.",
                           "", 0, 100);
        m_w->enableLink(b1, w1);


        // Show album thumbnails with albums ?
        b1 = m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "showcoverwithalb", 
                           "Show cover thumbnails in album lists",
                           "");
        pname = "coverwithalbwidth";
        if (!settings.contains(pname)) {
            settings.setValue(pname, 32);
        }
        w1 = m_w->addParam(idx, ConfTabsW::CFPT_INT, pname, 
                           "Album cover thumnbails in lists pixel width.",
                           "", 0, 900);
        m_w->enableLink(b1, w1);

        
        // Display albums as covers instead of lists
        b1 = m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "albumsascovers",
                           "Display albums as bunch of covers not lists", "");

        pname = "albumsascoverswidth";
        if (!settings.contains(pname)) {
            settings.setValue(pname, 100);
        }
        w1 = m_w->addParam(idx, ConfTabsW::CFPT_INT, pname, 
                           "Album cover image width when main album display.",
                           "", 0, 900);
        m_w->enableLink(b1, w1);


        // Server search by default
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "dfltserversearch",
                      "Default to server search in directory browser",
                      "Set the server search checkbox by default");

        m_w->endOfList(idx);


        idx = m_w->addPanel("UPnP");
        vector<string> adapters;
        UPnPP::getAdapterNames(adapters);
        QStringList qadapters;
        qadapters.push_back("");
        for (unsigned int i = 0; i < adapters.size(); i++) {
            qadapters.push_back(u8s2qs(adapters[i]));
        }
        // Specify network interface ?
        m_w->addParam(idx, ConfTabsW::CFPT_CSTR, "netifname",
                      "Network interface name (needs restart)",
                      "Specify network interface to use, or leave blank to "
                      "use the first or only appropriate interface",
                      0, 0, &qadapters);
        
        // Filter out non-openhome renderers?
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "ohonly",
                      "Only show OpenHome renderers",
                      "Only show OpenHome-capable renderers in "
                      "the selection dialog. Avoids Bubble UPnP server dups.");

        // Notify content directory updateid changes
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "monitorupdateid",
                      "Notify on Content Directory update",
                      "Show dialog when content directory state changes.");

        // 
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "noavtsetnext",
                      "Do not use AVTransport gapless functionality",
                      "Disables use of setNextAVTransportUri/Metadata.<br>"
                      "Useful with some buggy AVT renderers. Irrelevant "
                      "with OpenHome.");
        // 
        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "noprotoinfocheck",
                      "Do not check track formats against renderer capabilities",
                      "Disables checking the directory entry resource lines "
                      "against the renderer procolInfo data.<br>Useful if you"
                      "get error messages but the tracks are supposed to be "
                      "playable.");

        
        idx = m_w->addPanel("Last.FM");

        m_w->addParam(idx, ConfTabsW::CFPT_BOOL, "lastfmscrobble",
                      "Send track information to Last.FM", "");
        m_w->addParam(idx, ConfTabsW::CFPT_STR, "lastfmusername",
                      "Last.FM user name", "");
        m_w->addParam(idx, ConfTabsW::CFPT_STR, "lastfmpassword",
                      "Last.FM password (md5)", "");
        m_w->endOfList(idx);

        idx = m_w->addForeignPanel(new SortprefsW(m_w), "Directory Sorting");
        m_w->endOfList(idx);

        idx = m_w->addPanel("Misc.");
        m_w->addParam(idx, ConfTabsW::CFPT_INT, "loglevel",
                      "Log verbosity.", "", 0, 6);
        m_w->addParam(idx, ConfTabsW::CFPT_FN, "logfilename",
                      "Log file name.", "");
        m_w->addParam(idx, ConfTabsW::CFPT_INT, "upnploglevel",
                      "Lower UPnP library log verbosity.", "", 0, 6);
        m_w->addParam(idx, ConfTabsW::CFPT_FN, "upnplogfilename",
                      "Lower UPnP library log file name.", "");
        m_w->endOfList(idx);

        // Parameters for specific renderers. This pannel is special
        // because the parameter values are not global, but specific
        // to a given renderer.
        idx = m_w->addPanel("Renderer-specific parameters");

        // Find visible renderers so that the user can add from a list
        QStringList qrenderers;
        vector<UPnPDeviceDesc> devices = rendererList();
        for (const auto& device : devices) {
            qrenderers.append(u8s2qs(device.friendlyName));
        }

        
        ConfParamW *rdrs = m_w->addParam(idx, ConfTabsW::CFPT_CSTRL, "rdrsel",
                                         tr("Renderer selection."), tr(""),
                                         0, 0, &qrenderers);
        connect(rdrs, SIGNAL(currentTextChanged(const QString&)),
                this, SLOT(onCurrentRendererChanged(const QString&)));

        m_w->addBlurb(idx, tr(
                          "<p>The parameters below are set for the renderer "
                          "selected in the above list. They all need an "
                          "application restart."));
        ConfParamW *p = m_w->addParam(
            idx, ConfTabsW::CFPT_BOOL, "usepauseforunpause",
            tr("Use Pause command to unpause (instead of Play)."),
            tr("Some Naim streamers may need this"));
        p->setImmediate();
        m_rdrparams.push_back(p);
        m_w->endOfList(idx);
        
        connect(m_w, SIGNAL(sig_prefsChanged()), 
                this, SIGNAL(sig_prefsChanged()));
    }

    m_w->reloadPanels();
    m_w->show();
}

void UPPrefs::onShowPrefs(Tab tb)
{
    onShowPrefs();
    // The enum values and the tab order are currently identical, no
    // need for complication
    m_w->setCurrentIndex(tb);
}
