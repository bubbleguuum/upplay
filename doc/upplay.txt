= Upplay manual
:toc: left

== Overview

image::legend-upplay-main.png[]

== Player section

Play, Pause, Stop, Next, Previous, Seek, and Adjust volume. I would guess that
these don't need explanations.

Most elements displaying information about the current track have tooltips
which can display a bigger and more readable version.

== Playlist section

Playlist entries:

- Double-click entry to play
- Click entry, then click the play button to play
- Use right click clear/invert selection to help choosing selected tracks
- Select and drag-drop to reorder
- Right click and select 'Remove' to delete current selection
- Right click and select 'Sort by track number' to reorder tracks (useful
  when tracks from an album directory were sent in wrong order by the Media
  Server).

Playlist buttons at the bottom of the playlist area (right-bottom of the
screen):

- The two left playlist mode buttons select how the tracks are sequenced:
  normal (nothing selected), repeat, shuffle.
- The three buttons next on the right change playlist add modes, which
  determine how "Send to playlist" from the directory browser will affect
  the playlist. By default, this inserts after the current track. You can
  also choose to Append or Replace, and opt to immediately start play for
  the first of the new tracks.
- The sequential number toggle will show numbers in front of the
  entries.

You can clear the current playlist by clicking the broom icon above it on
the right.


=== Saved seek position mode (for version 1.3 and later)

If "Save seek position when switching tracks" is set in the preferences,
then, when double-clicking a track in the playlist, the play will start at
a saved seek position instead of the beginning.

- If the playlist is in repeat mode, the saved position will be the last
  place where the track was paused
- If the playlist is in normal mode, the saved position will be the last
  position played (before the previous track switch).

This can be useful for playing tracks in parallel for comparison purposes.

The "saved pause position" mode works with a single track, so play will
restart at the same position every time you double-click (repeat play of a
passage).


== Directory browser section

The list at the top is a hybrid path/search history section:

 - When you browse a directory tree by clicking on containers, it shows
   your location in the tree.
 - If you perform searches, your search history will be stacked at the end
   of the path. Searches are not nested, and always relative to the closest
   directory earlier in the path. If you click a directory inside the
   search results, it will get stacked, and any further searches will be
   relative to this directory, not the earlier one.

Left-Clicking a track title will send it to the playlist, according to the
current playlist add mode.

Left-Clicking a directory title will get you there.

Right-clicking a track title will propose the following actions:

 - `Back`
 - `Send to playlist`: same as left-clicking.
 - `Send all to playlist`: send all tracks from this directory.
 - `Send all from here to playlist`: this track and the following ones go to
   the playlist.
 
Right-clicking a directory (in the list or in the path) will propose the
following actions:

- `Back`
- `Send to playlist`: this is a recursive walk, it will send all tracks from
  the subtree to the playlist. Don't do this at the root of a big tree...
- `Open in new tab`.
- `Random play by tracks`: this is analogous to `Send to playlist`, but the
   list is shuffled, then sent to the Renderer by slices (of 30 tracks). The
   slicing is so that so you can use a very big list without swamping the
   renderer. 
- `Random play by groups`: this will play groups of tracks, corresponding to
  the lowest level of directories in the subtree, typically albums, but
  these may also be MinimServer Groups for example.
- `Stop random play`: if currently playing.

While in "Random play by group" mode, the playlist broom tooltip displays
the next group. Clicking the button will skip to it.

Right-clicking empty space will propose to go back (or up).

Mid-Clicking a directory from the entries list, or a search or directory in
the path at the top will open the element in a new tab. This can be
particularly useful if you want to go back to a previous search without
losing the current display.

`^f` or `/` will open the search panel, `Esc` will close it (or the x-cross
button at the right).

When long lists of albums or artists are displayed, an alphabet will appear
at the top. Clicking on a letter will bring you to the top of the
selected alphabetic section. Just typing the letter in the browser window
will do the same.

=== Directory browser display modes.

As of version 1.4.0, the appearance of containers and albums in the
directory browser can be adjusted from the File->Preferences->Application
tab.

By default containers are only displayed by their title.

- "Show some artist information in album lists" allows displaying some
  artist information (if it was sent by the media server). The artist
  information can be truncated to a chosen length.

- "Show cover thumbnails in album lists" will show a thumbnail of the album
  cover before the title. The image size can be adjusted with the next entry
  in the tab.

- "Display albums as bunchs of covers not lists" will display the
  containers or albums as a field of cover images. The image size can be
  adjusted with the next entry.

None of these choices affect the appearance of the item/track entries.


== Searching 

Upplay supports two search modes:
 
- Inside the current directory page: this is handy to quickly skip inside
  a long list.
- Server search.

The search panel is located at the bottom of the library window (the one on
the right), and normally hidden. You can open it by the following actions:
 
- Typing `/` inside the library window.
- Typing `Ctrl+f` inside the library window.
- Using the View menu

Not all servers support search (e.g, MediaTomb does not, Minidlna does,
etc.).

You can close the panel by the following actions:

- Typing `Esc` (the Escape key).
- Clicking the x-cross icon on the right of the panel
- Using the `View` menu.

When the search panel opens for the first time, the panel is in local
search mode. Typing inside the search entry area will try to find matches
in the page. You can use the two arrow buttons or type `F3` or `Shift+F3`
to find the next or previous match.

In server mode, the search panel will let you enter a search string and the
kind of field inside which you want to search (the combobox on the right,
Artist/Album/Title). Type `Enter` or click the search button to start the
search. 

The results are displayed just like a directory listing and can be
used in the same way (including opening tabs for directory listings, etc.).

Server searches are always relative to the first directory earlier in the
path, never to another search.
If your search yields nothing, one of the things to check is that you are
high enough in the tree (not searching a single directory).

Also *_space characters are significant in the search entry_*. This has a
capacity for trouble but it can also be a great help when trying to match
words (there are no standard UPnP ways to specify word matches, or anchor a
search at the beginning or end of a field).

== Tab bar section

Clicking the big plus sign will open a new tab. Clicking a tab's cross
sign will close it...

[[menus]]
== Menus

File:

- `Change Media Renderer` will let you do just this. You will see a dialog
  with the list of renderers currently seen on the network. After choosing
  one, you may decide that the current *upplay* playlist will replace the
  one or the new renderer, or be appended to it by checking the appropriate
  places. Double-clicking a renderer will get you there too.
- `Select Source` is only activated if the current Renderer supports
  OpenHome, and will let you select the Source among the available ones
  (`Playlist`, `Radio` etc.).
- `Save / Load playlist` will let you save the current playlist to a local
  file, or load one which you previously saved. Please note that this can
  only work with Media Servers which preserve URLs across database
  rebuilds. Specifically this does *not* work with *Minidlna* and
  *MediaTomb*. See xref:SAVED-PLAYLISTS[the more detailed note on this subject]
- `Open Songcast Tool` will open a dialog for managing the connections
  between Linn Songcast Senders or Receivers. link:songcast.html[more
  details here].
- `View`  lets you elect to hide the right panel (directory listing), change
   the color scheme for a dark one and other interesting experiences. Also
   a number of Preferences can be adjusted from the appropriately named
   submenu. 

And `Help`, there is no help...

[[keyboard]]
== Keyboard shortcuts

- Space/MediaTogglePlayPause/MediaPlay/MediaPause : toggle play/pause.
- Ctl+Right / MediaNext : next track.
- Ctl+Left / MediaPrevious : previous track.
- Ctl+Space : stop.    
- Ctl+Up / Plus : volume increase.
- Ctl+Down/ Minus : volume decrease.
- Right : seek forward a few secs.
- Left : seek backward a few secs.

[[hiresadjust]]
== Adjusting the font sizes on high resolution displays

Most people will find that the font sizes (and buttons/icons) are too small
on high DPI displays.

As of version 1.2.10, the font sizes can be adjusted from the preferences
menu ('view->preferences', 'Application Tab', 'Display Scale'). Just choose
a decimal scale factor.

[[SAVED-PLAYLISTS]]
== Issues in saving and restoring playlists

The Upplay file menu has an entry to save the current playlist to disk, and
load the saved playlist.

Each saved playlist element is comprised of the audio track URL and the
track metadata. These are the elements which need to be sent to the Media
Renderer for playing a given track.

Unfortunately, some Media Servers regenerate the audio track URLs in a more
or less random way when they regenerate their database (e.g. after the user
adds a new album).

When this happens, the saved URLs become invalid or can point to a
different audio track (the metadata: artist, title etc. is still valid of
course). So, when playing a restored playlist, the correct metadata will be
displayed, but the wrong audio, or no audio, will be playing.

It follows that these media servers are incompatible with the playlist
saving facility. This would be very complicated to fix.

The media servers which I know to preserve the URLs build them based on the
tracks' file system paths. This means that, even with these, the URLs could
be invalidated if you rearrange the file tree. One could imagine basing the
URLs on the file hashes, à la git, but, as far as I know, nobody does
things this way.

I don't have an exhaustive list of the servers which work or don't. For
now, just a few examples:

Media servers which do not preserve the URLs (incompatible with the
playlist saving facility):

- Minidlna
- MediaTomb

Media servers which do preserve the URLs:

- Minimserver
- Upmpdcli local Media Server (uprcl module).
