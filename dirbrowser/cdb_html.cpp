/* Copyright (C) 2005-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "cdbrowser.h"

#include <string>
#include <vector>

#include <QSettings>
#ifdef USING_WEBENGINE
#include <QLoggingCategory>
#endif
#include <libupnpp/upnpavutils.hxx>

#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Style.h"
#include "upadapt/upputils.h"
#include "utils/utf8iter.h"

using namespace std;
using namespace UPnPClient;
using namespace UPnPP;


// Notes for WebEngine
// - All links must begin with http:// for acceptNavigationRequest to be
//   called. 
// - The links passed to acceptNav.. have the host part 
//   lowercased -> we change S0 to http://h/S0, not http://S0

// The const part + the js we need for webengine. This is logically
// const after initialization by the first constructor.
static QString const_html_top;

// The const part + the js + changeable style, computed as needed.
static QString html_top;

// Initial post-header content of the servers page. The servers list
// will be inserted in the body using Javascript
static const QString empty_server_page_bot(
    "</head><body>"
    "<h2 id='cdstitle'>Content Directory Services</h2>"
    "</body></html>"
    );

// Content while we look for servers. 
static const QString init_server_page_bot(
    "</head><body>"
    "<h2 id='cdstitle'>Content Directory Services</h2>"
    "<p>&nbsp;</p><p>Looking for servers...</p>"
    "</body></html>"
    );


// Mark for the beginning of titles (used with search() to jump to
// first title with given initial).  We need a mark, else the search
// for the first title with given initial could match anywhere, not
// just at the beginning of a title.
//
// Wanted to use htab, but this is stripped in the final HTML
//
// \n is not, but search for \nword fails in webkit when not in the
// first cell (when the first cell has a thumbnail, so the title is in
// the second one). Works with the first cell, so probably a webkit
// bug. Does not work at all with webengine.
//
// Also tried an invisible string like utf-8 "word joiner", but it's
// ignored by both webengine and webkit.
// nbsp is treated as normal space.
// 
// So use a visible bullet because I see no other way
// Bullet-space
const string CDBrowser::CTTitleStartMarker("\xe2\x80\xa2 ");

// Once initialization of the constant part at the top of pages: read
// the javascript files.
static void init_const_html_top()
{
    const_html_top = u8s2qs(
        "<html><head><meta http-equiv='content-type' "
        "content='text/html; charset=utf-8'>"
        );
#ifdef USING_WEBENGINE
    QLoggingCategory("js").setEnabled(QtDebugMsg, true);
    QString jsfn = Helper::getCSSPath() + "containerscript.js";
    QString js = "<script type=\"text/javascript\">\n";
    js += QString::fromUtf8(Helper::readFileToByteArray(jsfn));
    js += "</script>\n";
    const_html_top += js;
#endif
}

void CDBrowser::init_HTML()
{
    if (const_html_top.isEmpty()) {
        init_const_html_top();
    }
    setStyleSheet(CSettingsStorage::getInstance()->getPlayerStyle(), false);
}

// Complete the top of pages by reading the CSS from our
// configuration, possibly fixing it for the current scale value, and
// appending it to const_html_top
void CDBrowser::setStyleSheet(bool dark, bool redisplay)
{
    QString cssfn = Helper::getCSSPath() + "cdbrowser.css";
    QString cssdata = QString::fromUtf8(Helper::readFileToByteArray(cssfn));

    float multiplier = QSettings().value("wholeuiscale").toFloat();
    if (multiplier == 0)
        multiplier = 1.0;

    cssdata = u8s2qs(Style::scale_fonts(qs2utf8s(cssdata), multiplier));
    if (dark) {
        cssfn = Helper::getCSSPath() + "dark.css";
    } else {
        cssfn = Helper::getCSSPath() + "standard.css";
    }
    cssdata +=  Helper::readFileToByteArray(cssfn);

    html_top = const_html_top;
    html_top += "<style>\n";
    html_top += cssdata;
    html_top += "</style>\n";
    if (redisplay) {
        refresh();
    }
}

// With message "looking for servers"
QString CDBrowser::initialServersPage()
{
    return QString(html_top + init_server_page_bot);
}

// To be filled with the current servers list
QString CDBrowser::emptyServersPage()
{
    return QString(html_top + empty_server_page_bot);
}

QString CDBrowser::DSToHtml(unsigned int idx, const UPnPDeviceDesc& dev)
{
    QString out;
    out += QString("<p class='cdserver' cdsid='%1'>").arg(idx);
    out += QString("<a href='http://h/S%1'>").arg(idx);
    out += QString::fromUtf8(dev.friendlyName.c_str());
    out += QString("</a></p>");
    return out;
}

QString CDBrowser::imgtag(const string& imgurl, int width)
{
    QString url = imgurl.empty() ? m_formatparams.genericAlbumImageURI :
        u8s2qs(imgurl);
    return QString("<img width='") + QString("%1").arg(width)+
        QString("' src='") + url + "'>";
}

static QString mkctattrs(const string& id, unsigned int idx, bool isPlaylist)
{
    return QString("class='container' objid='%1' objidx='%2' ispl='%3'").
        arg(u8s2qs(id)).arg(idx).arg(isPlaylist);
}

QString CDBrowser::CTToHtml(unsigned int idx, const UPnPDirObject& e)
{
    bool isPlaylist = !e.getprop("upnp:class").compare(plContainerClass);
    QString ctattrs = mkctattrs(e.m_id, idx, isPlaylist);
    
    string albumArtURI;
    if (m_formatparams.albumsAsCovers || m_formatparams.coverWithAlbum) {
        e.getprop("upnp:albumArtURI", albumArtURI);
    }
    string artist;
    if (m_formatparams.artistWithAlbum) {
        e.getprop("upnp:artist", artist);
        if (!artist.empty()) {
            utf8truncate(artist, m_formatparams.artistWithAlbumBytes,
                         UTF8T_ATWORD|UTF8T_ELLIPSIS, "\xE2\x80\xA6",
                         " \t\n\t,;.");
        }
    }
    
    if (m_formatparams.albumsAsCovers) {
        int imgw = m_formatparams.albumsAsCoversWidth *
            m_formatparams.scalemultiplier;
        string title = e.m_title;

        // Album title row. div with width limited to the cover image size.
        QString ttrow = QString("<tr %1><td>").arg(ctattrs) +
            QString(
                "<div class='ct_albcovertt' style='width:%1px;'>").arg(imgw) +
            QString("<a class='ct_title' href='http://h/C%1'>").arg(idx)  +
            u8s2qs(CTTitleStartMarker + Helper::escapeHtml(title)) +
            "</a></div></td></tr>";

        // Artist row. Same.
        QString artrow = QString("<tr %1><td>").arg(ctattrs) +
            QString(
                "<div class='ct_albcoverart' style='width:%1px;'>").arg(imgw) +
            u8s2qs(Helper::escapeHtml(artist)) + "</div></td></tr>";

        // Whole block: table with 3 rows: image, title, artist
        QString thumbhtml = QString("<table class='ct_albcovertable'>")  +
            QString("<tr %1><td>").arg(ctattrs) + 
            QString("<a href='http://h/C%1'>").arg(idx) +
            imgtag(albumArtURI, imgw) + "</a></td></tr>" +
            ttrow + artrow + "</table>";
        return thumbhtml;

    } else {
        // Displaying albums as lists, like tracks
        QString out = QString("<tr %1>").arg(ctattrs);

        if (m_formatparams.coverWithAlbum) {
            int imgw = m_formatparams.coverWithAlbumWidth *
                m_formatparams.scalemultiplier;
            out += QString("<td width='%1'>").arg(imgw+2);
            out += QString("<a class='ct_title' href='http://h/C%1'>").arg(idx);
            out += imgtag(albumArtURI, imgw);
            out += "</a></td>";
        } else {
            out += QString("<td></td>");
        }

        out += QString("<td><a class='ct_title' href='http://h/C%1'>").arg(idx);
        out += u8s2qs(CTTitleStartMarker + Helper::escapeHtml(e.m_title));
        out += "</a></td>";

        if (m_formatparams.artistWithAlbum) {
            out += "<td class='ct_artist'>";
            out += u8s2qs(Helper::escapeHtml(artist));
            out += "</td>";
        }

        out += "</tr>";
        return out;
    }
}

/** 
 * Show item as table row with 5 cells: tno, artist, title, album, duration
 * @arg idx index in entries array
 * @arg e array entry
*/
QString CDBrowser::ItemToHtml(unsigned int idx, const UPnPDirObject& e,
                              int maxartlen)
{
    QString out;
    string val;

    out = QString("<tr class='item' objid='%1' objidx='%2'>").
        arg(e.m_id.c_str()).arg(idx);

    e.getprop("upnp:originalTrackNumber", val);
    out += QString("<td class='tk_tracknum'>");
    if (val.size() == 0) {
        out += "&nbsp;";
    } else {
        out += QString::fromUtf8(Helper::escapeHtml(val).c_str());
    }
    out += "</td>";

    out += "<td class='tk_title'>";
    if (e.m_title.size() == 0) {
        out += "&nbsp;";
    } else {
        out += QString("<a href='http://h/I%1'>").arg(idx);
        out += QString::fromUtf8(Helper::escapeHtml(e.m_title).c_str());
        out += "</a>";
    }
    out += "</td>";

    val.clear();
    e.getprop("upnp:artist", val);
    out += "<td class='tk_artist'>";
    if (maxartlen > 0 && int(val.size()) > maxartlen) {
        int len = maxartlen-3 >= 0 ? maxartlen-3 : 0;
        val = val.substr(0,len) + "...";
    }
    if (val.size() == 0) {
        out += "&nbsp;";
    } else {
        out += QString::fromUtf8(Helper::escapeHtml(val).c_str());
    }
    out += "</td>";
    
    val.clear();
    e.getprop("upnp:album", val);
    out += "<td class='tk_album'>";
    out += QString::fromUtf8(Helper::escapeHtml(val).c_str());
    out += "</td>";
    
    val.clear();
    e.getrprop(0, "duration", val);
    int seconds = upnpdurationtos(val);
    // Format as mm:ss
    int mins = seconds / 60;
    int secs = seconds % 60;
    char sdur[100];
    sprintf(sdur, "%02d:%02d", mins, secs);
    out += "<td class='tk_duration'>";
    out += sdur;
    out += "</td>";

    out += "</tr>";

    return out;
}


// Container display page, next after top: close head and add body
// tag, possibly with an event listener for webengine.
static const QString init_container_pagemid = QString::fromUtf8(
#ifdef USING_WEBENGINE
    "</head><body onload=\"addEventListener('contextmenu', saveLoc)\">\n"
#else
    "</head><body>\n"
#endif
    );

// Container display page, table header for table version.
static const QString init_container_empty_table = QString::fromUtf8(
    "<table id='entrylist'>"
    "<colgroup>"
    "<col class='coltracknumber'>"
    "<col class='coltitle'>"
    "<col class='colartist'>"
    "<col class='colalbum'>"
    "<col class='colduration'>"
    "</colgroup>"
    "</table></body></html>"
    );

// Container display page: thumbnails display area. Stays empty if we
// are in list mode and do not display album thumbnails
static const QString init_container_thumbnails_div = QString::fromUtf8(
    "<div id='thumbnailsdiv'>"
    "</div>"
    );

// Container display alphabetic jump links
QString CDBrowser::alphalinks(const string& initials)
{
    QString html = QString("<div class='alphalist'>");
    html += "<br clear='all'/>";
    for (unsigned int i = 0; i < initials.size(); i++) {
        QString letter(1, initials[i]);
        html += QString("<a href='http://h/a") + letter +
            QString("'>") + letter + QString("</a>");
    }

    html += QString("</div>");
    return html;
}

// Container display page: header part before the table: current
// browse path and default alphalinks
QString CDBrowser::headerHtml()
{
    QString headerdata;

    headerdata += "<div id='browsepath'>\n";
    headerdata += "  <ul>\n";
    bool current_is_search = false;
    for (unsigned i = 0; i < m_curpath.size(); i++) {
        CtPathElt& e{m_curpath[i]};
        QString sep("&gt;");
        if (!e.searchStr.empty()) {
            if (current_is_search) {
                // Indicate that searches are not nested by changing
                // the separator
                sep = "&lt;&gt;";
            }
            current_is_search = true;
        }
        if (i == 0)
            sep = "";
        headerdata += QString(
            "   <li class='container' objid='%3' ispl='%5'>"
            " %4 <a href='http://h/L%1'>%2</a></li>\n").
            arg(i).arg(u8s2qs(e.title)).arg(u8s2qs(e.objid)).arg(sep)
            .arg(e.isPlaylist);
    }

    headerdata += "  </ul>\n";
    headerdata += "</div>\n";

    headerdata += alphalinks("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    
    return headerdata;
}

// Container display page: assemble the whole thing.
void CDBrowser::initContainerHtml(const string& ss)
{
    LOGDEB1("CDBrowser::initContainerHtml\n");
    QString header;
    header += "<div id='fixedheader'>\n";
    header += headerHtml();
    header += "</div>\n";
    // See comments in the CSS file
    header += "<div id='hiddenheader'>\n";
    header += headerHtml();
    header += "</div>\n";
    header += "<br clear='all'/>";

    if (!ss.empty()) {
        header += QString("Search results for: ") + 
            QString::fromUtf8(ss.c_str()) + "<br/>";
    }
    QString html = html_top + init_container_pagemid +
        header +
        init_container_thumbnails_div +
        init_container_empty_table;
    LOGDEB1("initContainerHtml: initial content: " << qs2utf8s(html) << endl);
    mySetHtml(html);
    LOGDEB1("CDBrowser::initContainerHtml done\n");
}
