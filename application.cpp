/* Copyright (C) 2013  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#include <QApplication>
#include <QMessageBox>
#include <QDir>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QScreen>

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/log.hxx"
#include "libupnpp/control/discovery.hxx"
#include "libupnpp/control/mediarenderer.hxx"
#include "libupnpp/control/renderingcontrol.hxx"
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
#include "libupnpp/control/conman.hxx"
#endif

#include "GUI/mainw/mainw.h"
#include "GUI/playlist/GUI_Playlist.h"
#include "GUI/prefs/prefs.h"
#include "GUI/renderchoose/renderchoose.h"
#include "GUI/sourcechoose/sourcechoose.h"
#include "GUI/songcast/songcastdlg.h"
#include "notifications/notifications.h"
#include "HelperStructs/CSettingsStorage.h"
#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "HelperStructs/globals.h"
#include "dirbrowser/dirbrowser.h"
#include "playlist/playlist.h"
#include "playlist/playlistavt.h"
#include "playlist/playlistnull.h"
#include "playlist/playlistohpl.h"
#include "playlist/playlistohrcv.h"
#include "playlist/playlistohrd.h"
#include "playlist/playlistlocrd.h"
#include "upadapt/avtadapt.h"
#include "upadapt/ohpladapt.h"
#include "upadapt/songcast.h"
#include "upadapt/rendererlist.h"
#include "upqo/ohproduct_qo.h"
#include "upqo/ohradio_qo.h"
#include "upqo/ohreceiver_qo.h"
#include "upqo/ohtime_qo.h"
#include "upqo/ohvolume_qo.h"
#include "upqo/renderingcontrol_qo.h"
#include "utils/writedescription.h"
#include "utils/smallut.h"

using namespace std;
using namespace UPnPClient;

float Application::horizontalDPI;

class Application::Internal {
public:
    Internal(QApplication *qapp)
        : m_app(qapp) {}
    
    GUI_Player   *m_player{0};
    std::shared_ptr<Playlist> m_playlist;
    DirBrowser   *m_cdb{0};

    UPnPClient::MRDH    m_rdr;
    RenderingControlQO *m_rdco{0};
    AVTPlayer    *m_avto{0};
    OHTimeQO     *m_ohtmo{0};
    OHVolumeQO   *m_ohvlo{0};
    OHProductQO  *m_ohpro{0};
    
    GUI_Playlist *m_ui_playlist{0};
    SongcastTool *m_sctool{0};
    UpplayNotifications *m_notifs{0};
    
    CSettingsStorage *m_settings{0};
    QApplication     *m_app;

    bool             m_initialized{false};
    // Can we send titles into the playlist (e.g. not OHradio).
    bool             m_playlistIsPlaylist{false};
    OHProductQO::SourceType m_ohsourcetype{OHProductQO::OHPR_SourceUnknown};
    QString          m_renderer_friendly_name;
};


#define CONNECT(a,b,c,d) m->m_app->connect(a, SIGNAL(b), c, SLOT(d),    \
                                           Qt::UniqueConnection)

static UPPrefs g_prefs;

static UPnPDeviceDirectory *superdir;

Application::Application(QApplication* qapp, QObject *parent)
    : QObject(parent)
{
    m = new Internal(qapp);
    m->m_settings = CSettingsStorage::getInstance();

    QString version = UPPLAY_VERSION;
    m->m_settings->setVersion(version);

    QScreen *screen = QApplication::primaryScreen();    
    horizontalDPI = screen->logicalDotsPerInchX();
    
    m->m_player = new GUI_Player(this);
    g_prefs.setParent(m->m_player);
    
    m->m_ui_playlist = new GUI_Playlist(m->m_player->getParentOfPlaylist(), 0);
    m->m_player->setPlaylistWidget(m->m_ui_playlist);

    m->m_cdb = new DirBrowser(m->m_player->getParentOfLibrary(), 0);
    m->m_player->setLibraryWidget(m->m_cdb);

    m->m_notifs = new UpplayNotifications(this);

    superdir = UPnPDeviceDirectory::getTheDir(1);
    if (superdir == 0) {
        cerr << "Can't create UPnP discovery object" << endl;
        exit(1);
    }

    init_connections();
    string uid = qs2utf8s(m->m_settings->getPlayerUID());
    if (uid.empty()) {
        QTimer::singleShot(0, this, SLOT(chooseRenderer()));
    } else {
        if (!setupRenderer(uid)) {
            cerr << "Can't connect to previous media renderer" << endl;
            QTimer::singleShot(0, this, SLOT(chooseRenderer()));
        }
    }

    m->m_player->setWindowTitle("Upplay " + version);
    m->m_player->setWindowIcon(QIcon(Helper::getIconPath("logo.png")));
    m->m_player->setPlaylist(m->m_ui_playlist);
    m->m_player->setStyle(m->m_settings->getPlayerStyle());
    m->m_player->show();

    m->m_ui_playlist->resize(m->m_player->getParentOfPlaylist()->size());

    m->m_player->ui_loaded();

    onPrefsChanged();
    m->m_initialized = true;
}

Application::~Application()
{
    delete m->m_player;
}

bool Application::is_initialized()
{
    return m->m_initialized;
}

// Used for testing the AVT interface on upmpdcli running with oh+avt
static bool avtonly = false;

void Application::chooseRenderer()
{
    MetaData md;
    getIdleMeta(&md);
    QSettings settings;
    bool ohonly = settings.value("ohonly").toBool();
    int flags{0};
    if (ohonly) {
        flags |= int(RSF_OHONLY);
    }
    if (avtonly) {
        flags |= int(RSF_AVTONLY);
    }
    vector<UPnPDeviceDesc> devices = rendererList(flags);
    emit sig_openhome_renderer(false);
    if (devices.empty()) {
        QMessageBox::warning(0, "Upplay", tr("No Media Renderers found."));
        return;
    }
    
    RenderChooseDLG dlg(m->m_player);
    if (ohonly) {
        dlg.setWindowTitle(tr("Select Renderer (OpenHome Only)"));
    }

    for (auto& device : devices) {
        QString fname = u8s2qs(device.friendlyName);
        if (!m->m_renderer_friendly_name.compare(fname)) {
            QListWidgetItem *item = new QListWidgetItem(fname);
            QFont font = dlg.rndsLW->font();
            font.setBold(true);
            item->setFont(font);
            dlg.rndsLW->addItem(item);
            dlg.rndsLW->setCurrentItem(item);
        } else {
            dlg.rndsLW->addItem(fname);
        }
    }
    if (!dlg.exec()) {
        return;
    }

    int row = dlg.rndsLW->currentRow();
    if (row < 0 || row >= int(devices.size())) {
        cerr << "Internal error: bad row after renderer choose dlg" << endl;
        return;
    }

    UPnPDeviceDesc& chosen(devices[row]);
    MetaDataList curmeta;
    if (m->m_playlist) {
        m->m_playlist->get_metadata(curmeta);
    }

    m->m_renderer_friendly_name = u8s2qs(chosen.friendlyName);
    if (!setupRenderer(chosen.UDN)) {
        QMessageBox::warning(0, "Upplay", tr("Can't connect to ") +
                             m->m_renderer_friendly_name);
        m->m_renderer_friendly_name = "";
        return;
    }
    m->m_settings->setPlayerUID(u8s2qs(chosen.UDN));

    if (m->m_playlist && !dlg.keepRB->isChecked()) {
        if (dlg.replRB->isChecked()) {
            m->m_playlist->psl_clear_playlist();
        }
        m->m_playlist->psl_add_tracks(curmeta);
    }
}

void Application::chooseSource()
{
    if (m->m_ohpro) {
        chooseSourceOH();
    } else {
        // Not ready yet
        return;
#if 0
        chooseSourceAVT();
#endif
    }
}

void Application::switchToRadio(bool onoff)
{
    if (!m->m_ohpro) {
        return;
    }
    vector<UPnPClient::OHProduct::Source> srcs;
    if (!m->m_ohpro->getSources(srcs)) {
        return;
    }
    LOGDEB("Application::chooseSource: got " << srcs.size() << " sources\n");
    int cur = -1;
    m->m_ohpro->sourceIndex(&cur);
    std::string cursrcname;
    if (cur >= 0 && cur < int(srcs.size())) {
        cursrcname = srcs[cur].name;
    }
    // Using the well known names for upmpdcli songcast sources, this
    // is brittle, but at worse, we'll switch to the non-songcast
    // source.
    bool isSongcast = cursrcname.find("Songcast") != std::string::npos;
    if (onoff) {
        if (isSongcast) {
            m->m_ohpro->setSourceIndexByName("RD-to-Songcast");
        } else {
            m->m_ohpro->setSourceIndexByName("Radio");
        }
    } else {
        if (isSongcast) {
            m->m_ohpro->setSourceIndexByName("PL-to-Songcast");
        } else {
            m->m_ohpro->setSourceIndexByName("Playlist");
        }
    }
}

void Application::chooseSourceOH()
{
    vector<UPnPClient::OHProduct::Source> srcs;
    if (!m->m_ohpro->getSources(srcs)) {
        return;
    }
    LOGDEB("Application::chooseSource: got " << srcs.size() << " sources\n");
    int cur = -1;
    m->m_ohpro->sourceIndex(&cur);

    vector<int> rowtoidx;
    SourceChooseDLG dlg(m->m_player);
    for (unsigned int i = 0; i < srcs.size(); i++) {
        // Receiver can't be usefully selected (no way to specify the
        // sender). Old versions of upmpdcli made Receiver not
        // visible, but the linn device have it visible and this is
        // necessary for kazoo, so just skip Receiver-type sources.
        if (!srcs[i].visible || srcs[i].type == "Receiver")
            continue;
        QString stype = u8s2qs(srcs[i].type + "\t(" + srcs[i].name + ")");
        if (int(i) == cur) {
            QListWidgetItem *item = new QListWidgetItem(stype);
            QFont font = dlg.rndsLW->font();
            font.setBold(true);
            item->setFont(font);
            dlg.rndsLW->addItem(item);
        } else {
            dlg.rndsLW->addItem(stype);
        }
        rowtoidx.push_back(i);
    }
    if (!dlg.exec()) {
        return;
    }

    int row = dlg.rndsLW->currentRow();
    if (row < 0 || row >= int(rowtoidx.size())) {
        LOGERR("Internal error: bad row after source choose dlg\n");
        return;
    }
    int idx = rowtoidx[row];
    if (idx != cur) {
        m->m_ohpro->setSourceIndex(idx);
    }
}


#if 0
// Avt radio is not ready yet, this is not used for now.
void Application::chooseSourceAVT()
{
    vector<int> rowtoidx;
    SourceChooseDLG dlg(m->m_player);
    dlg.rndsLW->addItem("Playlist");
    dlg.rndsLW->addItem("Radio");
    if (!dlg.exec()) {
        return;
    }
    int row = dlg.rndsLW->currentRow();
    if (m->m_playlist) {
        m->m_playlist->psl_stop();
    }
    m->m_player->stopped();
    if (row == 1) {
        QString fn = QDir(Helper::getSharePath()).filePath("radiolist.xml");
        m->m_playlist = shared_ptr<Playlist>(
            new PlaylistLOCRD(m->m_avto, fn.toLocal8Bit()));
        m->m_playlistIsPlaylist = false;
    } else {
        m->m_playlist = shared_ptr<Playlist>(
            new PlaylistAVT(m->m_avto, m->m_rdr->desc()->UDN));
        m->m_playlistIsPlaylist = true;
    }
    playlist_connections();
}
#endif

void Application::openSongcast()
{
    SongcastDLG *scdlg;
    if (!m->m_sctool) {
        scdlg = new SongcastDLG(m->m_player);
        m->m_sctool = new SongcastTool(scdlg, this);
    } else {
        scdlg = m->m_sctool->dlg();
        m->m_sctool->initControls();
    }
    if (scdlg) {
        scdlg->hide();
        scdlg->show();
    }
}

void Application::clear_renderer()
{
    m->m_rdr = UPnPClient::MRDH();
    deleteZ(m->m_rdco);
    deleteZ(m->m_avto);
    deleteZ(m->m_ohtmo);
    deleteZ(m->m_ohvlo);
    deleteZ(m->m_ohpro);
    m->m_ohsourcetype = OHProductQO::OHPR_SourceUnknown;
}

void Application::reconnectOrChoose()
{
    string uid = qs2utf8s(m->m_settings->getPlayerUID());
    if (uid.empty() || !setupRenderer(uid)) {
        clear_renderer();
        m->m_playlist = shared_ptr<Playlist>(new PlaylistNULL());
        m->m_playlistIsPlaylist = false;
        playlist_connections();
        if (QMessageBox::warning(0, "Upplay",
                                 tr("Connection to current rendererer lost. "
                                    "Choose again ?"),
                                 QMessageBox::Cancel | QMessageBox::Ok, 
                                 QMessageBox::Ok) == QMessageBox::Ok) {
            chooseRenderer();
        }
    }
}

bool Application::setupRenderer(const string& uid)
{
    clear_renderer();

    bool needs_playlist = true;
    
    // The media renderer object is not used directly except for
    // providing handles to the services. Note that the lib will
    // return anything implementing either renderingcontrol or
    // ohproduct
    m->m_rdr = getRenderer(uid, false);
    if (!m->m_rdr) {
        cerr << "Renderer " << uid << " not found" << endl;
        return false;
    }
    m->m_renderer_friendly_name = u8s2qs(m->m_rdr->desc()->friendlyName);

    bool needavt = true;
    OHPRH ohpr = m->m_rdr->ohpr();
    if (!avtonly && ohpr) {
        // This is an OpenHome media renderer
        m->m_ohpro = new OHProductQO(ohpr);
        connect(m->m_ohpro, SIGNAL(sourceTypeChanged(int)),
                this, SLOT(onSourceTypeChanged(int)));

        int scratch;
        if (!m->m_ohpro->sourceIndex(&scratch)) {
            // nobody there.
            LOGERR("setupRenderer: can't connect to OpenHome renderer" <<
                   qs2utf8s(m->m_renderer_friendly_name) << endl);
            return false;
        }

        // Try to use the time service
        OHTMH ohtm = m->m_rdr->ohtm();
        if (ohtm) {
            LOGDEB("Application::setupRenderer: OHTm ok, no need for avt\n");
            m->m_ohtmo = new OHTimeQO(ohtm);
            // no need for AVT then
            needavt = false;
        }

        // Create appropriate Playlist object depending on type of
        // source. Some may need m->m_ohtmo, so keep this behind its
        // creation
        createPlaylistForOpenHomeSource();
        needs_playlist = false;

        // Move this out of the if when avt radio is ready
        emit sig_openhome_renderer(true);
    } else {
        emit sig_openhome_renderer(false);
    }

    // It would be possible in theory to be connected to an openhome
    // playlist without a time service?? and use avt for time updates
    // instead.
    if (needavt) {
        AVTH avt = m->m_rdr->avt();
        if (!avt) {
            LOGERR("Renderer: AVTransport missing but we need it\n");
            return false;
        }
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
        m->m_avto = new AVTPlayer(avt, m->m_rdr->conman());
#else
        m->m_avto = new AVTPlayer(avt);
#endif
        if (m->m_avto == nullptr || !m->m_avto->checkConnection()) {
            return false;
        }
    }

    // Keep this after avt object creation !
    if (needs_playlist) {
        LOGDEB("Application::setupRenderer: using AVT playlist\n");
        m->m_playlist = shared_ptr<Playlist>(
            new PlaylistAVT(m->m_avto, m->m_rdr->desc()->UDN));
        m->m_playlistIsPlaylist = true;
    }


    // Use either renderingControl or ohvolume for volume control.
    RDCH rdc = m->m_rdr->rdc();
    if (rdc) {
        LOGDEB("Application::setupRenderer: using Rendering Control\n");
        m->m_rdco = new RenderingControlQO(rdc);
    } else {
        OHVLH ohvl = m->m_rdr->ohvl();
        if (!ohvl) {
            LOGERR("Device implements neither RenderingControl nor OHVolume\n");
            return false;
        }
        LOGDEB("Application::setupRenderer: using OHVolume\n");
        m->m_ohvlo =  new OHVolumeQO(ohvl);
    }
    
    renderer_connections();
    playlist_connections();

    return true;
}

void Application::createPlaylistForOpenHomeSource()
{
    m->m_ohsourcetype = OHProductQO::SourceType(m->m_ohpro->getSourceType());

    switch (m->m_ohsourcetype) {

    case OHProductQO::OHPR_SourceRadio:
    {
        OHRDH ohrd = m->m_rdr->ohrd();
        if (!ohrd) {
            LOGERR("Application::createPlaylistForOpenHomeSource: "
                   "radio mode, but can't connect\n");
            return;
        }
        OHIFH ohif = m->m_rdr->ohif();
        m->m_playlist = shared_ptr<Playlist>(
            new PlaylistOHRD(new OHRad(ohrd), ohif ? new OHInf(ohif) : 0));
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(true);
    }
    break;

    case OHProductQO::OHPR_SourceReceiver:
    {
        OHRCH ohrc = m->m_rdr->ohrc();
        if (!ohrc) {
            LOGERR("Application::createPlaylistForOpenHomeSource: "
                   "receiver mode, but can't connect\n");
            return;
        }
        m->m_playlist = shared_ptr<Playlist>(
            new PlaylistOHRCV(ohrc, u8s2qs(m->m_rdr->desc()->friendlyName)));
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(false);
    }
    break;

    case OHProductQO::OHPR_SourcePlaylist:
    {
        OHPLH ohpl = m->m_rdr->ohpl();
        if (ohpl) {
            m->m_playlist = shared_ptr<Playlist>(
                new PlaylistOHPL(new OHPlayer(ohpl), m->m_ohtmo));
            m->m_playlistIsPlaylist = true;
        }
        emit sig_openhome_source_radio(false);
    }
    break;

    default:
    {
        m->m_playlist = shared_ptr<Playlist>(new PlaylistNULL());
        m->m_playlistIsPlaylist = false;
        emit sig_openhome_source_radio(false);
    }
    break;
    }

    if (!m->m_playlist) {
        LOGERR("Application::createPlaylistForOpenHomeSource: "
               "could not create playlist object\n");
    }
}

void Application::onDirSortOrder()
{
    g_prefs.onShowPrefs(UPPrefs::PTAB_DIRSORT);
}

void Application::onSourceTypeChanged(int tp)
{
    LOGDEB1("Application::onSourceTypeChanged: " << int(tp) << endl);
    if (tp == m->m_ohsourcetype) {
        LOGDEB1("Application::onSourceTypeChanged: same type\n");
        return;
    }
    
    if (!m->m_ohpro) {
        // Not possible cause ohpro is the sender of this signal.. anyway
        LOGERR("Application::onSourceTypeChanged: no OHProduct!!\n");
        return;
    }
    createPlaylistForOpenHomeSource();
    playlist_connections();
}

void Application::getIdleMeta(MetaData* mdp)
{
    QString sourcetype;
    if (m->m_ohpro) {
        vector<OHProduct::Source> sources;
        if (m->m_ohpro->getSources(sources)) {
            int idx;
            if (m->m_ohpro->sourceIndex(&idx)) {
                if (idx >= 0 && idx < int(sources.size())) {
                    sourcetype = u8s2qs(sources[idx].name);
                }
            }
        }
    }

    mdp->title = QString::fromUtf8("Upplay ") + UPPLAY_VERSION;
    if (m->m_renderer_friendly_name.isEmpty()) {
        mdp->artist = "No renderer connected";
    } else {
        mdp->artist = tr("Renderer: ") + m->m_renderer_friendly_name;
        if (!sourcetype.isEmpty()) {
            mdp->artist += QString::fromUtf8(" (") + sourcetype + ")";
        }
    }
}

// We may switch the playlist when an openhome renderer switches sources. So
// set the playlist connections in a separate function
void Application::playlist_connections()
{
    if (m->m_playlistIsPlaylist)
        m->m_cdb->setPlaylist(m->m_playlist);
    else
        m->m_cdb->setPlaylist(shared_ptr<Playlist>());

    // Use either ohtime or avt for time updates
    if (m->m_ohtmo) {
        CONNECT(m->m_ohtmo, secsInSongChanged(quint32),
                m->m_playlist.get(), onRemoteSecsInSong(quint32));
        CONNECT(m->m_ohtmo, secsInSongChanged(quint32),
                m->m_notifs, songProgress(quint32));
    } else if (m->m_avto) {
        CONNECT(m->m_avto, secsInSongChanged(quint32),
                m->m_playlist.get(), onRemoteSecsInSong(quint32));
        CONNECT(m->m_avto, secsInSongChanged(quint32),
                m->m_notifs, songProgress(quint32));
    }

    CONNECT(m->m_player, play(), m->m_playlist.get(), psl_play());
    CONNECT(m->m_player, pause(), m->m_playlist.get(), psl_pause());
    CONNECT(m->m_player, stop(), m->m_playlist.get(), psl_stop());
    CONNECT(m->m_player, forward(), m->m_playlist.get(), psl_forward());
    CONNECT(m->m_player, backward(), m->m_playlist.get(), psl_backward());
    CONNECT(m->m_player, sig_load_playlist(),
            m->m_playlist.get(), psl_load_playlist());
    CONNECT(m->m_player, sig_save_playlist(),
            m->m_playlist.get(), psl_save_playlist());
    CONNECT(m->m_player, sig_seek(int), m->m_playlist.get(), psl_seek(int));

    CONNECT(m->m_playlist.get(), connectionLost(), this, reconnectOrChoose());
    CONNECT(m->m_playlist.get(), playlistModeChanged(Playlist_Mode),
            m->m_ui_playlist, setPlayerMode(Playlist_Mode));
    CONNECT(m->m_playlist.get(), sig_track_metadata(const MetaData&),
            m->m_player, update_track(const MetaData&));

    CONNECT(m->m_playlist.get(), sig_track_metadata(const MetaData&),
            m->m_notifs, notify(const MetaData&));
    CONNECT(m->m_playlist.get(), sig_stopped(),  m->m_notifs, onStopped());
    CONNECT(m->m_playlist.get(), sig_paused(),  m->m_notifs, onPaused());
    CONNECT(m->m_playlist.get(), sig_playing(),  m->m_notifs, onPlaying());
    CONNECT(m->m_notifs, notifyNeeded(const MetaData&),
            m->m_player, onNotify(const MetaData&));
            
    CONNECT(m->m_playlist.get(), sig_stopped(),  m->m_player, stopped());
    CONNECT(m->m_playlist.get(), sig_paused(),  m->m_player, paused());
    CONNECT(m->m_playlist.get(), sig_playing(),  m->m_player, playing());
    CONNECT(m->m_playlist.get(), sig_playing_track_changed(int),
            m->m_ui_playlist, track_changed(int));
    CONNECT(m->m_playlist.get(), sig_playlist_updated(MetaDataList&, int, int),
            m->m_ui_playlist, fillPlaylist(MetaDataList&, int, int));
    CONNECT(m->m_ui_playlist, selection_min_row(int),
            m->m_playlist.get(), psl_selection_min_row(int));
    CONNECT(m->m_ui_playlist, playlist_mode_changed(const Playlist_Mode&),
            m->m_playlist.get(), psl_change_mode(const Playlist_Mode&));
    CONNECT(m->m_ui_playlist, dropped_tracks(const MetaDataList&, int),
            m->m_playlist.get(), psl_insert_tracks(const MetaDataList&, int));
    CONNECT(m->m_ui_playlist, sig_rows_removed(const QList<int>&, bool),
            m->m_playlist.get(), psl_remove_rows(const QList<int>&, bool));
    CONNECT(m->m_ui_playlist, sig_sort_tno(),
            m->m_playlist.get(), psl_sort_by_tno());
    CONNECT(m->m_ui_playlist, row_activated(int),
            m->m_playlist.get(), psl_change_track(int));
    CONNECT(m->m_ui_playlist, clear_playlist(),
            m->m_playlist.get(), psl_clear_playlist());

    m->m_playlist->update_state();
}

// Direct renderer-Player connections (not going through the Playlist):
// volume and time mostly.
void Application::renderer_connections()
{
    CONNECT(m->m_player, sig_dumpDescription(),
            this, writeDescription());
    // Use either ohtime or avt for time updates
    if (m->m_ohtmo) {
        CONNECT(m->m_ohtmo, secsInSongChanged(quint32),
                m->m_player, setCurrentPosition(quint32));
    } else if (m->m_avto) {
        CONNECT(m->m_avto, secsInSongChanged(quint32),
                m->m_player, setCurrentPosition(quint32));
    }
    if (m->m_ohvlo) {
        CONNECT(m->m_player, sig_volume_changed(int), m->m_ohvlo, setVolume(int));
        CONNECT(m->m_player, sig_mute(bool), m->m_ohvlo, setMute(bool));
        CONNECT(m->m_ohvlo, volumeChanged(int), m->m_player, setVolumeUi(int));
        CONNECT(m->m_ohvlo, muteChanged(bool), m->m_player, setMuteUi(bool));
        // Set up the initial volume from the renderer value
        m->m_player->setVolumeUi(m->m_ohvlo->volume());
    } else if (m->m_rdco) {
        CONNECT(m->m_player, sig_volume_changed(int), m->m_rdco, setVolume(int));
        CONNECT(m->m_player, sig_mute(bool), m->m_rdco, setMute(bool));
        CONNECT(m->m_rdco, volumeChanged(int), m->m_player, setVolumeUi(int));
        CONNECT(m->m_rdco, muteChanged(bool), m->m_player, setMuteUi(bool));
        // Set up the initial volume from the renderer value
        m->m_player->setVolumeUi(m->m_rdco->volume());
    }
}

void Application::onPrefsChanged()
{
    QSettings settings;

    static string oldlogfilename;
    string logfilename("stderr");
    const char *cp;
    if ((cp = getenv("UPPLAY_LOGFILENAME"))) {
        logfilename = cp;
    } else if (settings.contains("logfilename")) {
        logfilename = qs2utf8s(settings.value("logfilename").toString());
    }
    int loglevel = Logger::LLINF;
    if ((cp = getenv("UPPLAY_LOGLEVEL"))) {
        loglevel = atoi(cp);
    } else if (settings.contains("loglevel")) {
        loglevel = settings.value("loglevel").toInt();
    }
    cerr << "Setting log file name " << logfilename << " verb " << loglevel
         << endl;
    if (oldlogfilename.compare(logfilename)) {
        Logger::getTheLog(logfilename)->reopen(logfilename);
        oldlogfilename = logfilename;
    }
    Logger::getTheLog(logfilename)->setLogLevel(Logger::LogLevel(loglevel));

    m->m_cdb->refresh();
    // The point size have been adjusted in the style sheet, and Qt
    // will deal with the actual DPI for text. But we need to at least
    // scale the album cover image size. Dealing with the Webkit view
    // DPI issues is done in dirbrowser.cpp
    auto dark = m->m_settings->getPlayerStyle();
    m->m_player->setStyle(dark);
    float scale = settings.value("wholeuiscale").toFloat();
    if (scale == 0)
        scale = 1.0;
    LOGDEB("onPrefsChanged: wholeuiscale: " << scale <<
           " horizontalDPI " << horizontalDPI << endl);
    float multiplier = (scale * horizontalDPI) / 96.0;
    m->m_player->scale(multiplier);
}

// Connections which make sense without a renderer.
void Application::init_connections()
{
    CONNECT(m->m_player, show_small_playlist_items(bool),
            m->m_ui_playlist, psl_show_small_playlist_items(bool));
    CONNECT(m->m_player, sig_choose_renderer(), this, chooseRenderer());
    CONNECT(m->m_player, sig_open_songcast(), this, openSongcast());
    CONNECT(m->m_player, sig_choose_source(), this, chooseSource());
    CONNECT(m->m_player, sig_skin_changed(bool), m->m_cdb, refresh());
    CONNECT(m->m_player, showSearchPanel(bool), m->m_cdb, showSearchPanel(bool));
    CONNECT(m->m_player, sig_quitting(), m->m_cdb, onAboutToExit());
    CONNECT(m->m_player, sig_preferences(), &g_prefs, onShowPrefs());
    CONNECT(&g_prefs, sig_prefsChanged(), this, onPrefsChanged());
    CONNECT(m->m_cdb, sig_next_group_html(QString),
            m->m_ui_playlist, psl_next_group_html(QString));
    CONNECT(m->m_cdb, sig_sort_order(), this, onDirSortOrder());
    CONNECT(m->m_player, sig_sortprefs(), m->m_cdb, onSortprefs());
    CONNECT(this, sig_openhome_renderer(bool),
            m->m_player, psl_openhome_renderer(bool));
    CONNECT(this, sig_openhome_source_radio(bool),
            m->m_ui_playlist, psl_openhome_source_radio(bool));
    CONNECT(m->m_ui_playlist, sig_radio(bool), this, switchToRadio(bool));
}

void Application::writeDescription()
{
    if (!superdir || !m || !m->m_rdr) {
        return;
    }

    string deviceXML;
    unordered_map<string, string> srvsXML;
    const string& devname(m->m_rdr->desc()->UDN);
    if (!superdir->getDescriptionDocuments(devname,
                                           deviceXML, srvsXML)) {
        QMessageBox::warning(0, "Upplay",
                             tr("Could not retrieve description documents"));
        return;
    }
    QString dir = QFileDialog::getExistingDirectory(
        m->m_player, tr("Directory for description files"));
    if (dir.isEmpty()) {
        return;
    }
    if (!writeDescriptionFiles(qs2utf8s(dir), devname, deviceXML, srvsXML)) {
        QMessageBox::warning(0, "Upplay",
                             tr("Could not write description files."));
    }
}
