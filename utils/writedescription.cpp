/* Copyright (C) 2017 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "writedescription.h"

#include <stdio.h>
#include <iostream>

using namespace std;

static void neutchars(const string& str, string& out, const string& chars)
{
    string::size_type startPos, pos;
    out.clear();
    for (pos = 0;;) {
        // Skip initial chars, break if this eats all.
        if ((startPos = str.find_first_not_of(chars, pos)) == string::npos) {
            break;
        }
        // Find next delimiter or end of string (end of token)
        pos = str.find_first_of(chars, startPos);
        // Add token to the output. Note: token cant be empty here
        if (pos == string::npos) {
            out += str.substr(startPos);
        } else {
            out += str.substr(startPos, pos - startPos) + "_";
        }
    }
}

static bool make_file(const string& nm, const string& content)
{
    FILE *fp = fopen(nm.c_str(), "wb");
    if (nullptr == fp) {
        cerr << "Could not create/open " << nm << endl;
        perror("fopen");
        return false;
    }
    if (fwrite(content.c_str(), content.size(), 1, fp) != 1) {
        fclose(fp);
        cerr << "Could not write to  " << nm << endl;
        perror("fwrite");
        return false;
    }
    fclose(fp);
    return true;
}


bool writeDescriptionFiles(
    const string& dirname, const string& devname, const string& deviceXML,
    const unordered_map<string, string>& srvsXML)
{
    string path, fn, fn1;
    fn = devname + "-description.xml";
    neutchars(fn, fn1, "/ \n\r\t");
    path = dirname + "/" + fn1;
    if (!make_file(path, deviceXML)) {
        return false;
    }
    for (auto entry : srvsXML) {
        fn = entry.first + ".xml";
        neutchars(fn, fn1, "/ \n\r\t");
        path = dirname + "/" + fn1;
        if (!make_file(path, entry.second)) {
            return false;
        }
    }        
    return true;
}

