/* 
 * Copyright (C) 2017  J.F. Dockes
 * Copyright (C) 2013  Lucio Carreras
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <string>
#include <memory>

#include <QObject>
#include <QApplication>

struct MetaData;

class Application : public QObject
{
    Q_OBJECT;

public:
    Application(QApplication* qapp, QObject *parent = 0);
    virtual ~Application();

    bool is_initialized();
    void getIdleMeta(MetaData* mdp);
    static float horizontalDPI;
                            
public slots:
    void chooseRenderer();
    void chooseSource();
    void openSongcast();
    void reconnectOrChoose();
    void onSourceTypeChanged(int);
    void onDirSortOrder();
    void onPrefsChanged();
    void writeDescription();
    void switchToRadio(bool);
    
signals:
    void sig_openhome_renderer(bool);
    void sig_openhome_source_radio(bool);
    
private:
    class Internal;
    Internal *m;

    void clear_renderer();
    void init_connections();
    void renderer_connections();
    void playlist_connections();
    bool setupRenderer(const std::string& uid);
    void createPlaylistForOpenHomeSource();
    void chooseSourceOH();
    void chooseSourceAVT();
};


#endif // APPLICATION_H
