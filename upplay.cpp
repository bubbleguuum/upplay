/* Copyright (C) 2014 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <iostream>
using namespace std;

#include <QApplication>
#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QSettings>
#include <QMessageBox>

#include <libupnpp/upnpplib.hxx>
#include <libupnpp/upnpputils.hxx>
#include <libupnpp/log.hxx>

#include "application.h"
#include "upadapt/upputils.h"

using namespace UPnPP;

static const char *thisprog;

static int    op_flags;
#define OPT_h     0x4 
#define OPT_c     0x20
#define OPT_v     0x40

static const char usage [] =
    "upplay [-h] [-v] : options: get help and version\n"
    ;

static void
versionInfo(FILE *fp)
{
    fprintf(fp, "Upplay %s %s\n",
           UPPLAY_VERSION, LibUPnP::versionString().c_str());
}

static void
Usage(void)
{
    FILE *fp = (op_flags & OPT_h) ? stdout : stderr;
    fprintf(fp, "%s: Usage: %s", thisprog, usage);
    versionInfo(fp);
    exit((op_flags & OPT_h)==0);
}

static QString libiniterror(LibUPnP *mylib)
{
#if LIBUPNPP_AT_LEAST(0,19,4)
    Q_UNUSED(mylib);
    return u8s2qs(LibUPnP::errAsString(" ", LibUPnP::getInitError()));
#else
    if (mylib) {
        return u8s2qs(mylib->errAsString("main", mylib->getInitError()));
    } else {
        return QString();
    }
#endif
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("Upmpd.org");
    QCoreApplication::setApplicationName("upplay");

    // Avoid disturbing argc and argv. Especially, setting argc to 0
    // prevents WM_CLASS to be set from argv[0] (it appears that qt
    // keeps a ref to argc, and that it is used at exec() time to set
    // WM_CLASS from argv[0]). Curiously, it seems that the argv
    // pointer can be modified without consequences, but we use a copy
    // to play it safe
    int myargc = argc;
    char **myargv = argv;

    thisprog = myargv[0];
    myargc--; myargv++;
    while (myargc > 0 && **myargv == '-') {
        (*myargv)++;
        if (!(**myargv))
            Usage();
        while (**myargv)
            switch (*(*myargv)++) {
            case 'h':   op_flags |= OPT_h; Usage(); break;
            case 'v':   op_flags |= OPT_v; versionInfo(stdout); exit(0); break;
            default: Usage();
            }
        myargc--; myargv++;
    }

    if (myargc > 0)
        Usage();

    if (Logger::getTheLog("stderr") == 0) {
        cerr << "Can't initialize log" << endl;
        return 1;
    }

    QSettings settings;

#if LIBUPNPP_AT_LEAST(0,19,2)
    int upnploglevel = settings.value("upnploglevel").toInt();
    string suplfn = qs2utf8s(
        settings.value("upnplogfilename").toString().trimmed());
    const char *upnplogfilename = suplfn.c_str();
    char *cp = getenv("UPPLAY_UPNPLOGFILENAME");
    if (cp)
        upnplogfilename = cp;
    cp = getenv("UPPLAY_UPNPLOGLEVEL");
    if (cp) {
        upnploglevel = atoi(cp);
    }
    upnploglevel = std::max(upnploglevel, 0);
    if (upnploglevel) {
        LibUPnP::setLogFileName(upnplogfilename,LibUPnP::LogLevel(upnploglevel));
    }
#endif

    string ifname = qs2utf8s(settings.value("netifname").toString().trimmed());
    if (!ifname.empty()) {
        cerr << "Initializing library with interface " << ifname << endl;
    }

    // Note that the lib init may fail here if ifname is wrong.
    // The later discovery would call the lib init again (because
    // the singleton is still null), which would retry the init,
    // without an interface this time, which would probably succeed,
    // so that things may still mostly work, which is confusing and the
    // reason we do the retry here instead.
    LibUPnP *mylib = LibUPnP::getLibUPnP(false, 0, ifname);
    if (!mylib || !mylib->ok()) {
        QString explain = libiniterror(mylib);
        if (ifname.empty()) {
            QMessageBox::warning(0, "Upplay", app.tr("Lib init failed: ") +
                explain);
        } else {
            QMessageBox::warning(
                0, "Upplay", app.tr("Lib init failed for ") + u8s2qs(ifname) +
                " " + explain + app.tr(". Retrying with null interface"));
            mylib = LibUPnP::getLibUPnP();
            if (!mylib || !mylib->ok()) {
                explain = libiniterror(mylib);
                QMessageBox::warning(
                    0, "Upplay", app.tr("Lib init failed again. "
                                        "Network configuration issue ? " ) +
                    explain);
            }
        }
    }

#if !LIBUPNPP_AT_LEAST(0,19,2)
    int upnploglevel = settings.value("upnploglevel").toInt();
    string suplfn = qs2utf8s(
        settings.value("upnplogfilename").toString().trimmed());
    const char *upnplogfilename = suplfn.c_str();
    char *cp = getenv("UPPLAY_UPNPLOGFILENAME");
    if (cp)
        upnplogfilename = cp;
    cp = getenv("UPPLAY_UPNPLOGLEVEL");
    if (cp) {
        upnploglevel = atoi(cp);
    }
    upnploglevel = std::max(upnploglevel, 0);
    if (upnploglevel) {
        mylib->setLogFileName(upnplogfilename,LibUPnP::LogLevel(upnploglevel));
    }
#endif

    Application application(&app);
    return app.exec();
}
