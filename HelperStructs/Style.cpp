/* Copyright (C) 2011  Lucio Carreras
 * Copyright (C) 2017 J.F. Dockes
 *
 * This file is part of Upplay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>
#include <QDebug>

#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "upadapt/upputils.h"
#include "utils/smallut.h"

#include <iostream>
#include <string>

// Programs built with gcc 4.8.4 (e.g.: Ubuntu Trusty), crash at
// startup while initializing stdc++ regular expression objects (and
// also crash if we make them non-static).
#if defined(__clang__) || defined(_WIN32) || __GNUC__ > 4
#define USE_REGEX 
#endif

#ifdef USE_REGEX
#include <regex>
/* font-size: 10pt; */
static const std::string fntsz_exp(
    R"((\s*font-size\s*:\s*)([0-9]+)(pt\s*;\s*))"
    );

/* Note: this one is not really useful any more now that we use a hidden div to 
   vertically size the header area: the top margin is currently 0
   body {margin: 20px 0px 0px 0px;}
*/
static const std::string bdmrg_exp(
    R"((\s*body\s*\{\s*margin:\s*)([0-9]+)(px\s*[0-9]+px\s*[0-9]+px\s*[0-9]+px\s*;\s*\}))"
    );
static std::regex fntsz_regex(fntsz_exp);
static std::regex bdmrg_regex(bdmrg_exp);
#endif // USE_REGEX

#include <array>
#include <math.h>

using namespace std;

QString Style::get_style(bool dark, float multiplier)
{
#if !defined(Q_OS_MACOS) && !defined(Q_OS_MAC)
    QString dir = Helper::getSharePath();
#else
    QString dir = Helper::getSharePath() + "/Resources/";
#endif

    QString commonstyle;
    Helper::read_file_into_str(dir + "/common.qss", &commonstyle);

    commonstyle = u8s2qs(scale_fonts(qs2utf8s(commonstyle), multiplier));
    
    QString style;
    if (!dark) {
        Helper::read_file_into_str(dir + "/standard.qss", &style);
    } else {
        Helper::read_file_into_str(dir + "/dark.qss", &style);
    }

    return commonstyle + style;
}


string Style::scale_fonts(const string& style, float multiplier)
{
    //cerr << "Style::scale_fonts: multiplier: " << multiplier << endl;
    vector<string> lines;
    stringToTokens(style, lines, "\n");
#ifdef USE_REGEX
    for (unsigned int ln = 0; ln < lines.size(); ln++) {
        const string& line = lines[ln];
        smatch m;
        if (regex_match(line, m, fntsz_regex) && m.size() == 4) {
            //cerr << "Got match (sz " << m.size() << ") for " << line << endl;
            int fs = atoi(m[2].str().c_str());
            int nfs = round(fs * multiplier);
            char buf[20];
            snprintf(buf, 20, "%d", nfs);
            lines[ln] = m[1].str() + buf + m[3].str();
            //cerr << "New line: [" << lines[ln] << "]\n";
        }
        m = smatch();
        if (regex_match(line, m, bdmrg_regex) && m.size() == 4) {
            //cerr << "Got match (sz " << m.size() << ") for " << line << endl;
            int fs = atoi(m[2].str().c_str());
            int nfs = ceil(fs * multiplier);
            char buf[20];
            snprintf(buf, 20, "%d", nfs);
            lines[ln] = m[1].str() + buf + m[3].str();
            //cerr << "New line: [" << lines[ln] << "]\n";
        }
    }
#endif    
    string nstyle = string();
    for (auto& ln : lines) {
        nstyle += ln + "\n";
    }
    return nstyle;
}
