/* Copyright (C) 2005 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "cdbrowser.h"

#ifdef _MSC_VER
#include <io.h>
#else
#include <unistd.h>
#endif

#include <fcntl.h>

#include <iostream>
#include <functional>

#include "utils/smallut.h"

#include <QUrl>
#ifdef USING_WEBENGINE
// Notes for WebEngine
// - All links must begin with http:// for acceptNavigationRequest to be
//   called. 
// - The links passed to acceptNav.. have the host part 
//   lowercased -> we change S0 to http://h/S0, not http://S0
// As far as I can see, 2016-09, two relatively small issues remaining:
//  - No way to know when page load is done (see mySetHtml())
//  - No way to save/restore the scroll position, all the rest works ok.
// The scroll issue is now, 2018-09, solved:
//   See https://bugreports.qt.io/browse/QTBUG-48323
//   QWebEngineView()->page()->runJavaScript(QString("window.scrollTo(%1, %2);").arg(scrollX).arg(scrollY));

#include <QWebEnginePage>
#include <QWebEngineSettings>
#include <QtWebEngineWidgets>
#include <QLoggingCategory>
#else
#include <QWebFrame>
#include <QWebSettings>
#include <QWebElement>
#endif

#include <QMessageBox>
#include <QMenu>
#include <QApplication>
#include <QByteArray>
#include <QProgressDialog>
#include <QClipboard>
#include <QFileDialog>

#include "HelperStructs/Helper.h"
#include "HelperStructs/Style.h"
#include "HelperStructs/CSettingsStorage.h"

#include "libupnpp/log.hxx"
#include "libupnpp/control/discovery.hxx"

#include "upqo/cdirectory_qo.h"
#include "upadapt/upputils.h"
#include "utils/md5.h"
#include "dirbrowser.h"
#include "rreaper.h"


using namespace std;
using namespace std::placeholders;
using namespace UPnPP;
using namespace UPnPClient;

static const string minimFoldersViewPrefix("0$folders");

const std::string
CDBrowser::plContainerClass{"object.container.playlistContainer"};

void CDWebPage::javaScriptConsoleMessage(
#ifdef USING_WEBENGINE
    JavaScriptConsoleMessageLevel,
#endif
    const QString& msg, int lineNum, const QString&)
{
    Q_UNUSED(msg);
    Q_UNUSED(lineNum);
    LOGDEB("JAVASCRIPT: "<< qs2utf8s(msg) << " at line " << lineNum << endl);
}

CDBrowser::CDBrowser(QWidget* parent)
    : QWEBVIEW(parent), m_reader(0), m_reaper(0), m_progressD(0),
      m_browsers(0), m_lastbutton(Qt::LeftButton), m_sysUpdId(0)
{
    init_HTML();
    setPage(new CDWebPage(this));
    
#ifdef USING_WEBENGINE
    connect(this, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinished(bool)));
#else
    connect(this, SIGNAL(linkClicked(const QUrl &)), 
            this, SLOT(onLinkClicked(const QUrl &)));
    // Not available and not sure that this is needed with webengine ?
    connect(page()->mainFrame(), SIGNAL(contentsSizeChanged(const QSize&)),
            this, SLOT(onContentsSizeChanged(const QSize&)));
    page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
#endif

    settings()->setAttribute(QWEBSETTINGS::JavascriptEnabled, true);
    if (parent) {
        settings()->setFontSize(QWEBSETTINGS::DefaultFontSize, 
                                parent->font().pointSize()+4);
        settings()->setFontFamily(QWEBSETTINGS::StandardFont, 
                                  parent->font().family());
    }
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(createPopupMenu(const QPoint&)));
    m_timer.setSingleShot(1);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(initialPage()));
    m_timer.start(0);
}

CDBrowser::~CDBrowser()
{
    deleteReaders("~CDBrowser");
}

void CDBrowser::mySetHtml(const QString& html)
{
    LOGDEB0("CDBrowser::mySetHtml:\n");
    LOGDEB1("            HTML :\n"<< qs2utf8s(html) << endl);

    const static QUrl baseUrl("file:///");
    setHtml(html, baseUrl);

#ifdef USING_WEBENGINE
    LOGDEB0("CDBrowser::setHtml returned, waiting for loaded event\n");
    m_pageloaded = false;
    while (!m_pageloaded) {
        qApp->processEvents();
    }
    LOGDEB0("CDBrowser::setHtml: page is loaded\n");
#if (QT_VERSION < QT_VERSION_CHECK(5, 11, 0))
    // For some reason this is needed to ensure that the js is inited.
    // Else we get the dreaded:
    // 'Uncaught ReferenceError: Base64Decode is not defined at line 2'
    Helper::msleep(100);
#endif
#endif
}

#ifdef USING_WEBENGINE
void CDBrowser::onLoadFinished(bool ok)
{
    Q_UNUSED(ok);
    LOGDEB("ONLOADFINISHED: " << ok << endl);
    // If ok is false, this is just a consequence of delegating a link
    // click, and has nothing to do with our setting html, ignore. See:
    // https://doc.qt.io/qt-5/qwebenginepage.html#acceptNavigationRequest
    if (ok) {
        m_pageloaded = true;
    }
}

vector<CharFlags> navreqtypes{
    CHARFLAGENTRY(QWebEnginePage::NavigationTypeLinkClicked),
        CHARFLAGENTRY(QWebEnginePage::NavigationTypeTyped),
        CHARFLAGENTRY(QWebEnginePage::NavigationTypeFormSubmitted),
        CHARFLAGENTRY(QWebEnginePage::NavigationTypeBackForward),
        CHARFLAGENTRY(QWebEnginePage::NavigationTypeReload),
        CHARFLAGENTRY(QWebEnginePage::NavigationTypeOther),
        };

bool CDWebPage::acceptNavigationRequest(const QUrl& url,
                                         NavigationType navtype,
                                         bool)
{
    LOGDEB1("CDWebPage::acceptNavigationRequest: type: " <<
            valToString(navreqtypes, navtype) << endl);

    if (navtype == QWebEnginePage::NavigationTypeLinkClicked) {
        QTimer::singleShot(
            0, [this, url]() {this->m_browser->onLinkClicked(url);});
        return false;
    }
    return true;
}

bool CDBrowser::eventFilter(QObject *object, QEvent *event)
{
    switch (event->type()) {
    case QEvent::MouseButtonRelease:
    {
        const QMouseEvent *mouseEvent(static_cast<QMouseEvent*>(event));
        if (mouseEvent) {
            m_lastbutton = mouseEvent->button();
        }
    }
    break;
    default:
        break;
    }
    return QObject::eventFilter(object, event);
}

void CDBrowser::onContentsSizeChanged(const QSize&)
{
    LOGDEB0("CDBrowser::onContentsSizeChanged\n");
    QString js = "window.scrollBy(" + 
        QString::number(m_savedscrollpos.x())+ ", " +
        QString::number(m_savedscrollpos.y()) + ");";
    runJS(js);
}

static QString base64_encode(QString string)
{
    return QByteArray(string.toUtf8()).toBase64();
}

void CDBrowser::onJSRan(const QVariant&)
{
    LOGDEB1("CDBrowser::onJSRan\n");
    emit sig_script_ran();
}

void CDBrowser::onStartScript(const QString& js)
{
    page()->runJavaScript(js, [this](const QVariant &v) {onJSRan(v);});
}

void CDBrowser::runJS(const QString& js)
{
    LOGDEB0("CDBrowser::runJS: " << qs2utf8s(js).substr(0, 50) << endl);
    m_jsbusy = true;
    QEventLoop loop;
    QObject::connect(this, SIGNAL(sig_script_ran()), &loop, SLOT(quit()));
    // Schedule the js in 0 seconds but not right now
    QTimer::singleShot(0, [this, js]() {onStartScript(js);});
    // The event loop would block until onScriptEnded slot is executed
    loop.exec();
    LOGDEB0("runJS: done: " << qs2utf8s(js).substr(0, 50) << endl);
    m_jsbusy = false;
}

#else // WEBKIT->

void CDBrowser::onContentsSizeChanged(const QSize&)
{
    page()->mainFrame()->setScrollPosition(m_savedscrollpos);
}

void CDBrowser::mouseReleaseEvent(QMouseEvent *event)
{
    //qDebug() << "CDBrowser::mouseReleaseEvent";
    m_lastbutton = event->button();
    QWEBVIEW::mouseReleaseEvent(event);
}

// Alphabetic key presses have the same effect as clicking one of the
// alphabet links.
void CDBrowser::keyPressEvent(QKeyEvent *e)
{
    /* We happen to know that the Key_X event value is the ascii character */
    unsigned char val =(unsigned char)e->key();
    if (val >= 'A' && val <= 'Z') {
        QString surl("http://h/a");
        surl += QString(val);
        QUrl url(surl);
        onLinkClicked(url);
    }
    QWEBVIEW::keyPressEvent(e);
}

void CDBrowser::runJS(const QString& js)
{
    LOGDEB0("CDBrowser::runJS: " << qs2utf8s(js).substr(0,50) << endl);
    page()->mainFrame()->evaluateJavaScript(js);
}

#endif // WEBKIT


void CDBrowser::appendHtml(const QString& elt_id, const QString& html)
{
    LOGDEB0("CDBrowser::appendHtml: elt_id [" << qs2utf8s(elt_id) << "]\n");
    LOGDEB1("CDBrowser::appendHtml: HTML [" << qs2utf8s(html) << "]\n");

#ifdef USING_WEBENGINE
    // With webengine, we can't access the qt object from the page, so
    // we do everything in js. The data is encoded as base64 to avoid
    // quoting issues, which induces complicated stuff for converting
    // to dom string, see containerscript.js for references.
    QString js("var morehtml = '" + base64_encode(html) + "';\n");
    js += QString("var decoded = Base64Decode(morehtml);\n");
    if (elt_id.isEmpty()) {
        js += QString("document.body.innerHTML += decoded;\n");
    } else {
        js += QString("document.getElementById(\"%1\").innerHTML += "
                     "decoded;\n").arg(elt_id);
    }
#else
    // With webkit we connect a Qt object with the browser and access
    // it directly from javascript.
    QWebFrame *mainframe = page()->mainFrame();
    StringObj morehtml(html);

    mainframe->addToJavaScriptWindowObject("morehtml", &morehtml, 
                                           SCRIPTOWNERSHIP);
    QString js;
    if (elt_id.isEmpty()) {
        js = QString("document.body.innerHTML += morehtml.text");
    } else {
        js = QString("document.getElementById(\"%1\").innerHTML += "
                     "morehtml.text").arg(elt_id);
    }
#endif
    runJS(js);
}

bool CDBrowser::newCds(int cdsidx)
{
    if (cdsidx > int(m_msdescs.size())) {
        LOGERR("CDBrowser::newCds: bad link index: " << cdsidx 
               << " cd count: " << m_msdescs.size() << endl);
        return false;
    }
    MSRH ms = MSRH(new MediaServer(m_msdescs[cdsidx]));
    if (!ms) {
        LOGERR("CDBrowser::newCds: MediaServer connect failed\n");
        return false;
    }
    CDSH cds = ms->cds();
    if (!cds) {
        LOGERR("CDBrowser::newCds: null cds" << endl);
        return false;
    }
    m_cds = std::shared_ptr<ContentDirectoryQO>(new ContentDirectoryQO(cds));
    m_sysUpdId = 0;
    connect(m_cds.get(), SIGNAL(systemUpdateIDChanged(int)),
            this, SLOT(onSysUpdIdChanged(int)));
    m_cds->srv()->getSearchCapabilities(m_searchcaps);
    emit sig_searchcaps_changed();
    return true;
}

void CDBrowser::onSysUpdIdChanged(int id)
{
    LOGDEB("CDBrowser::onSysUpdIdChanged: mine " << m_sysUpdId <<
           "server" << id << endl);

    if (!QSettings().value("monitorupdateid").toBool()) {
        return;
    }

    // 1st time is free
    if (!m_sysUpdId) {
        m_sysUpdId = id;
        return;
    }

    // We should try to use the containerUpdateIDs to make sure of
    // what needs to be done, instead of dumping the problem on the
    // user.
    if (m_sysUpdId != id) {

        m_sysUpdId = id;

        // CDSKIND_BUBBLE, CDSKIND_MEDIATOMB,
        // CDSKIND_MINIDLNA, CDSKIND_MINIM, CDSKIND_TWONKY
        ContentDirectory::ServiceKind kind = m_cds->srv()->getKind();
        switch (kind) {
            // Not too sure which actually invalidate their
            // tree. Pretty sure that Minim does not (we might just
            // want to reload the current dir).
        case ContentDirectory::CDSKIND_MINIDLNA: break;
        case ContentDirectory::CDSKIND_MEDIATOMB: break;
            // By default, don't do anything by default because some
            // cds keep changing their global updateid. We'd need to
            // check the containerUpdateID.
        case ContentDirectory::CDSKIND_MINIM:
        default: return;
        }

        QMessageBox::Button rep = 
            QMessageBox::question(0, "Upplay",
                                  tr("Content Directory Server state changed, "
                                     "some references may be invalid. (some "
                                     "playlist elements may be invalid too). "
                                     "<br>Reset browse state ?"),
                                  QMessageBox::Ok | QMessageBox::Cancel);
        if (rep == QMessageBox::Ok) {
            curpathClicked(1);
            m_sysUpdId = 0;
        }
    }
}

QPoint CDBrowser::scrollpos()
{
#ifdef USING_WEBENGINE
    QPointF fscrollpos = page()->scrollPosition();
#if defined(Q_OS_MACOS) || defined(Q_OS_MAC)
    // Mac only ? Does not seem to be wanted/needed on X11:
    //   Magic fudge factor (4/3)! The points returned by
    //   page()->scrollPosition() are three quarter from those
    //   needed later by javascript!
    fscrollpos *= 4./3.;
#endif // MAC
    return fscrollpos.toPoint();
#else
    return page()->mainFrame()->scrollPosition();
#endif
}

// This is connected to GUI signals. We need to separate the
// implementation in processOnLinkClicked() for counting automatic
// calls (when seeing a container with a single container entry for
// example, and to avoid unlimited recursion on a pathologic tree).
void CDBrowser::onLinkClicked(const QUrl &url)
{
    m_autoclickcnt = 0;
    deleteReaders("onLinkClicked event");
    processOnLinkClicked(url);
}

void CDBrowser::processOnLinkClicked(const QUrl &url)
{
    m_timer.stop();
    string scurl = qs2utf8s(url.toString());
    LOGDEB("CDBrowser::processOnLinkClicked: " << scurl << 
           " button " <<  m_lastbutton << " mid " << Qt::MidButton << endl);
    // Get rid of http://
    if (scurl.find("http://h/") != 0) {
        LOGERR("CDBrowser::onLinkClicked: bad link ! : " <<
               qs2utf8s(url.toString()));
        return;
    }
    scurl = scurl.substr(9);
    LOGDEB0("CDBrowser::onLinkClicked: corrected url: " << scurl << endl);

    int what = scurl[0];

    switch (what) {

    case 'a':
    {
        char initial = scurl[1];
        auto it = m_alphamap.find(initial);
        if (it != m_alphamap.end()) {
            m_browsers->doSearch(u8s2qs(it->second), false);
        }
    }
    break;

    case 'S':
    {
        // Servers page server link click: browse clicked server root
        unsigned int cdsidx = atoi(scurl.c_str()+1);
        m_curpath.clear();
        m_curpath.push_back(CtPathElt("0", "(servers)", false));
        if (!newCds(cdsidx)) {
            return;
        }
        browseContainer("0", m_msdescs[cdsidx].friendlyName, false);
    }
    break;

    case 'I':
    {
        // Directory listing item link clicked: add to playlist
        unsigned int i = atoi(scurl.c_str()+1);
        if (i > m_entries.size()) {
            LOGERR("CDBrowser::onLinkClicked: bad objid index: " << i 
                   << " id count: " << m_entries.size() << endl);
            return;
        }
        MetaDataList mdl;
        mdl.resize(1);
        if (!udirentToMetadata(&m_entries[i], &mdl[0], m_browsers->get_pl())) {
            QMessageBox::warning(0, "Upplay", "No compatible audio format");
        }
        emit sig_tracks_to_playlist(mdl);
    }
    break;

    case 'C':
    {
        // Directory listing container link clicked: browse subdir.
        m_curpath.back().scrollpos = scrollpos();
        unsigned int i = atoi(scurl.c_str() + 1);
        if (i > m_entries.size()) {
            LOGERR("CDBrowser::onLinkClicked: bad objid index: " << i 
                   << " id count: " << m_entries.size() << endl);
            return;
        }
        UPnPClient::UPnPDirObject& e{m_entries[i]};
        bool isPlaylist = !e.getprop("upnp:class").compare(plContainerClass);
        if (m_lastbutton == Qt::MidButton) {
            // Open in new tab
            vector<CtPathElt> npath(m_curpath);
            npath.push_back(CtPathElt(e.m_id, e.m_title, isPlaylist));
            emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()),
                                       npath);
        } else {
            browseContainer(e.m_id, e.m_title, isPlaylist);
        }
        return;
    }
    break;

    case 'L':
    {
        // Click in curpath section.
        unsigned int i = atoi(scurl.c_str()+1);
        curpathClicked(i);
        return;
    }
    break;

    default:
        LOGERR("CDBrowser::onLinkClicked: bad link type: " << what << endl);
        return;
    }
}

void CDBrowser::curpathClicked(unsigned int i)
{
    LOGDEB1("CDBrowser::curpathClicked: " << i << " pathsize " << 
            m_curpath.size() << endl);
    if (m_reader || m_reaper) {
        LOGDEB("CDBrowser::curpathClicked: already active\n");
        return;
    }
    if (i >= m_curpath.size()) {
        LOGERR("CDBrowser::curPathClicked: bad curpath index: " << i 
               << " path count: " << m_curpath.size() << endl);
        return;
    }

    if (i == 0) {
        m_curpath.clear();
        m_msdescs.clear();
        m_cds.reset();
        initialPage(true);
    } else {
        CtPathElt e{m_curpath[i]};
        m_curpath.erase(m_curpath.begin() + i, m_curpath.end());
        if (m_lastbutton == Qt::MidButton) {
            vector<CtPathElt> npath(m_curpath);
            npath.push_back(e);
            emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()),
                                       npath);
        } else {
            if (e.searchStr.empty()) {
                browseContainer(e.objid, e.title, e.isPlaylist, e.scrollpos);
            } else {
                search(e.objid, e.searchStr, e.scrollpos);
            }
        }
    }
}

int CDBrowser::serverFromUDN(const string& UDN)
{
    for (unsigned int i = 0; i < m_msdescs.size(); i++) {
        if (m_msdescs[i].UDN == UDN) {
            return int(i);
        }
    }
    return -1;
}

// This is called for browsing a specific container in a newly created
// tab.  We let initialPage handle the hard work.
void CDBrowser::browseInNewTab(QString UDN, vector<CtPathElt> path)
{
    LOGDEB1("CDBrowser::browseinnewtab: " << qs2utf8s(UDN) << endl);

    m_curpath = path;
    m_initUDN = UDN;
    m_timer.stop();
    m_timer.start(0);
    return;
}

// Re-browse (because sort criteria changed, or other prefs)
// if m_cds is not set, let initialPage() do its thing
void CDBrowser::refresh()
{
    LOGDEB("CDBrowser::refresh\n");
    setStyleSheet(CSettingsStorage::getInstance()->getPlayerStyle(), false);
    if (m_cds && m_curpath.size() >= 1) {
        curpathClicked(m_curpath.size() - 1);
    } else {
        m_forceRedisplay = true;
    }
}

void CDBrowser::updateFormatParams()
{
    QSettings settings;
    m_formatparams.artistWithAlbum =
        settings.value("showartwithalb", true).toBool();
    m_formatparams.artistWithAlbumBytes =
        settings.value("artwithalblen", 20).toInt();
    m_formatparams.coverWithAlbum =
        settings.value("showcoverwithalb", false).toBool();
    m_formatparams.coverWithAlbumWidth =
        settings.value("coverwithalbwidth", 20).toInt();
    m_formatparams.albumsAsCovers =
        settings.value("albumsascovers", false).toBool();
    m_formatparams.albumsAsCoversWidth =
        settings.value("albumsascoverswidth", 100).toInt();
    m_formatparams.genericAlbumImageURI =
        QString("qrc:/icons/generic-cover.jpg");
    m_formatparams.scalemultiplier = QSettings().value("wholeuiscale", 1.0).toFloat();
    if (m_formatparams.scalemultiplier == 0)
        m_formatparams.scalemultiplier = 1.0;
}

void CDBrowser::browseContainer(string ctid, string cttitle, bool ispl,
                                QPoint scrollpos)
{
    LOGDEB("CDBrowser::browseContainer: " << " ctid " << ctid << endl);
    if (m_reader || m_reaper) {
        LOGDEB("CDBrowser::browseContainer: already active\n");
        return;
    }

    updateFormatParams();

    emit sig_now_in(this, QString::fromUtf8(cttitle.c_str()));

    m_savedscrollpos = scrollpos;
    if (!m_cds) {
        LOGERR("CDBrowser::browseContainer: server not set" << endl);
        return;
    }
    m_entries.clear();

    m_curpath.push_back(CtPathElt(ctid, cttitle, ispl));

    initContainerHtml();
    
    m_reader = new CDBrowseQO(m_cds->srv(), ctid, string(), this);

    connect(m_reader, SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent*)),
            this, SLOT(onSliceAvailable(UPnPClient::UPnPDirContent *)));
    connect(m_reader, SIGNAL(done(int)), this, SLOT(onBrowseDone()));
    m_reader->start();
}

void CDBrowser::search(const string& iss)
{
    search(m_curpath.back().objid, iss);
}

void CDBrowser::search(const string& objid, const string& iss, QPoint scrollpos)
{
    deleteReaders("search");
    if (iss.empty())
        return;
    if (!m_cds) {
        LOGERR("CDBrowser::search: server not set" << endl);
        return;
    }

    updateFormatParams();

    m_savedscrollpos = scrollpos;
    m_entries.clear();
    m_curpath.push_back(CtPathElt(objid, "(search)", false, iss));
    initContainerHtml(iss);

    m_reader = new CDBrowseQO(m_cds->srv(), m_curpath.back().objid, iss, this);

    connect(m_reader, SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent*)),
            this, SLOT(onSliceAvailable(UPnPClient::UPnPDirContent *)));
    connect(m_reader, SIGNAL(done(int)), this, SLOT(onBrowseDone()));
    m_reader->start();
}


void CDBrowser::deleteReaders(const QString& who)
{
    Q_UNUSED(who);
//    qDebug() << "deleteReaders(): from " << who;
    if (m_reader) {
        m_reader->setCancel();
        m_reader->wait();
        delete m_reader;
        m_reader = 0;
    }
    if (m_reaper) {
        m_reaper->setCancel();
        m_reaper->wait();
        delete m_reaper;
        m_reaper = 0;
    }
    // Give a chance to queued signals (a thread could have queued
    // something before/while we're cancelling it). Else, typically
    // onSliceAvailable could be called at a later time and mess the
    // display.
    qApp->processEvents();
}

void CDBrowser::updateAlphamap(char& curinitial, const string& tt)
{
    char ninit = ::toupper(tt[0]);
    if (ninit != curinitial && ninit >= 'A' && ninit <= 'Z') {
        m_alphamap[ninit] = CTTitleStartMarker + tt;
        curinitial = ninit;
    }
}

// There are 2 instances of the alphalink because of the fixed header
// variable height double div trick (see cdb_html.cpp and the css).
static const char *setalphalinks_js =
    "var x = document.getElementsByClassName('alphalist');\n"
    "var i;\n"
    "var l = x.length;\n"
    "for (i = 0; i < l; i++) {\n"
    "  x[i].style.visibility = \"%1\";\n"
    "  x[i].innerHTML = \"%2\";\n"
    "}\n";

void CDBrowser::maybeShowAlphamap(unsigned int nct)
{
    LOGDEB1("CDBrowser::maybeShowAlphamap\n");
    if (nct >= 30 && m_alphamap.size() > 1) {
        string initials;
        for (auto& ent: m_alphamap) {
            if  (ent.first >= 'A' && ent.first <= 'Z') {
                initials += ent.first;
            }
        }
        runJS(QString(setalphalinks_js).
              arg("visible").arg(alphalinks(initials)));
    } else {
        runJS(QString(setalphalinks_js).arg("hidden").arg(""));
    }
}

void CDBrowser::onSliceAvailable(UPnPDirContent *dc)
{
    LOGDEB0("CDBrowser::onSliceAvailable\n");
    if (!m_reader) {
        LOGERR("CDBrowser::onSliceAvailable: no reader\n");
        // Cancelled.
        delete dc;
        return;
    }
    QString html;

    m_entries.reserve(m_entries.size() + dc->m_containers.size() + 
                      dc->m_items.size());

    for (auto& entry: dc->m_containers) {
        //qDebug() << "Container: " << it->dump().c_str();;
        m_entries.push_back(entry);
        html += CTToHtml(m_entries.size()-1, entry);
    }

    if (m_formatparams.albumsAsCovers) {
        appendHtml("thumbnailsdiv", html);
        html = "";
    }

    QSettings settings;
    int maxartlen = 0;
    if (settings.value("truncateartistindir").toBool()) {
        maxartlen = settings.value("truncateartistlen").toInt();
    }

    for (std::vector<UPnPDirObject>::iterator it = dc->m_items.begin();
         it != dc->m_items.end(); it++) {
        //qDebug() << "Item: " << it->dump().c_str();;
        m_entries.push_back(*it);
        html += ItemToHtml(m_entries.size()-1, *it, maxartlen);
    }
    appendHtml("entrylist", html);
    delete dc;
    // On very big lists (thousands), this can be quite late against
    // the dir reading threads, with a long queue of slice events
    // accumulated. The UI is frozen while we get called for each
    // event in the queue. We'd like to give a chance to user
    // interaction (e.g. scrolling)here, but it seems that there is no
    // way to process only the user events, so processEvents does not help.
    // Probably the only workable approach would be to do the
    // appending from another thread?
    // (by the way, processEvent if it worked must be called at the
    // bottom of the func else we risk onBrowseDone getting called and
    // deleting the reader before we can use it).
    //qApp->processEvents();
}

class DirObjCmp {
public:
    DirObjCmp(const vector<string>& crits)
        : m_crits(crits) {}
    bool operator()(const UPnPDirObject& o1, const UPnPDirObject& o2) {
        int rel;
        string s1, s2;
        for (unsigned int i = 0; i < m_crits.size(); i++) {
            const string& crit = m_crits[i];
            rel = dirobgetprop(o1, crit, s1).compare(
                dirobgetprop(o2, crit, s2));
            if (rel < 0)
                return true;
            else if (rel > 0)
                return false;
        }
        return false;
    }
    const string& dirobgetprop(const UPnPDirObject& o, const string& nm,
        string& storage) {
        if (!nm.compare("dc:title")) {
            return o.m_title;
        } else if (!nm.compare("uri")) {
            if (o.m_resources.size() == 0)
                return nullstr;
            return o.m_resources[0].m_uri;
        } 
        const string& prop = o.getprop(nm);
        if (!nm.compare("upnp:originalTrackNumber")) {
            char num[30];
            int i = atoi(prop.c_str());
            sprintf(num, "%010d", i);
            storage = num;
            return storage;
        } else if (!nm.compare("upplay:ctpath")) {
            //qDebug() << "TTPATH: " <<
            //    RecursiveReaper::ttpathPrintable(prop).c_str();
            return prop;
        } else {
            return prop;
        }
    }
    vector<string> m_crits;
    static string nullstr;
};
string DirObjCmp::nullstr;

void CDBrowser::onBrowseDone()
{
    LOGDEB0("CDBrowser::onBrowseDone: " << m_entries.size() << " entries\n");

#ifdef USING_WEBENGINE
    if (m_jsbusy) {
        QTimer::singleShot(100, this, SLOT(onBrowseDone()));
        return;
    }
#endif

    bool inPlaylist = m_curpath.back().isPlaylist;
    if (!m_reader) {
        LOGDEB("CDBrowser::onBrowseDone(int) no reader: cancelled\n");
        return;
    }
    m_reader->wait();
    deleteReaders("onBrowseDone");
    if (m_autoclickcnt++ < 3 && m_entries.size() == 1 &&
        m_entries[0].m_type == UPnPDirObject::container) {
        QUrl cturl(QString("http://h/C%1").arg(0));
        processOnLinkClicked(cturl);
        return;
    }
        
    vector<string> sortcrits;
    int sortkind = CSettingsStorage::getInstance()->getSortKind();
    if (sortkind == CSettingsStorage::SK_MINIMFNORDER && 
        m_reader->getKind() == ContentDirectory::CDSKIND_MINIM && 
        m_curpath.back().searchStr.empty() &&
        m_reader->getObjid().compare(0, minimFoldersViewPrefix.size(),
                                     minimFoldersViewPrefix) == 0) {
        sortcrits.push_back("uri");
    } else if (sortkind == CSettingsStorage::SK_CUSTOM) {
        QStringList qcrits = CSettingsStorage::getInstance()->getSortCrits();
        for (int i = 0; i < qcrits.size(); i++) {
            sortcrits.push_back(qs2utf8s(qcrits[i]));
        }
    }
    int nct = 0;
    char curinitial = 0;
    m_alphamap.clear();
    if (!inPlaylist && !sortcrits.empty()) {
        DirObjCmp cmpo(sortcrits);

        sort(m_entries.begin(), m_entries.end(), cmpo);
        initContainerHtml();
        QString tmbhtml, lsthtml;
        int maxartlen = 0;
        QSettings settings;
        if (settings.value("truncateartistindir").toBool()) {
            maxartlen = settings.value("truncateartistlen").toInt();
        }
        for (unsigned i = 0; i < m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::container) {
                updateAlphamap(curinitial, m_entries[i].m_title);
                if (m_formatparams.albumsAsCovers) {
                    tmbhtml += CTToHtml(i, m_entries[i]);
                } else {
                    lsthtml += CTToHtml(i, m_entries[i]);
                }
                nct++;
            } else {
                lsthtml += ItemToHtml(i, m_entries[i], maxartlen);
            }
        }
        if (!tmbhtml.isEmpty()) {
            appendHtml("thumbnailsdiv", tmbhtml);
        }
        if (!lsthtml.isEmpty()) {
            appendHtml("entrylist", lsthtml);
        }
    } else {
        for (unsigned i = 0; i < m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::container) {
                updateAlphamap(curinitial, m_entries[i].m_title);
                nct++;
            }
        }
    }
    maybeShowAlphamap(nct);

#ifdef USING_WEBENGINE
    // Don't see a way to set the scroll position in the page from
    // within WEBENGINE, thus do it by javascript
    //
    // The conversion from QPoint to QString looks ugly! Is there a more
    // elegant way?
    QString js = "window.scrollTo(" +
        QString::number(m_savedscrollpos.x())+ ", " +
        QString::number(m_savedscrollpos.y()) + ");";
    runJS(js);
#else
    page()->mainFrame()->setScrollPosition(m_savedscrollpos);
#endif

}


void CDBrowser::initialPage(bool redisplay)
{
    if (m_forceRedisplay) {
        redisplay = true;
        m_forceRedisplay = false;
    }
    LOGDEB0("CDBrowser::initialPage. redisplay " << redisplay <<
            " m_initUDN [" << qs2utf8s(m_initUDN) <<
            "] curpath size " << m_curpath.size() << endl);

#ifdef USING_WEBENGINE
    if (focusWidget()) {
        focusWidget()->removeEventFilter(this);
        focusWidget()->installEventFilter(this);
    }
#endif

    deleteReaders("initialPage");
    emit sig_now_in(this, tr("Servers"));
    m_searchcaps.clear();
    emit sig_searchcaps_changed();

    vector<UPnPDeviceDesc> msdescs;
    if (!MediaServer::getDeviceDescs(msdescs)) {
        LOGERR("CDBrowser::initialPage: getDeviceDescs failed" << endl);
    }
    if (msdescs.size() == 0) {
        LOGDEB("CDBrowser::initialPage: calling setHtml with 'looking for'\n");
        mySetHtml(initialServersPage());
        m_timer.start(1000);
        return;
    }
        
    LOGDEB1("CDBrowser::initialPage: " << msdescs.size() << " servers\n");

    // Check if servers list changed
    bool same = msdescs.size() == m_msdescs.size();
    if (same) {
        for (unsigned i = 0; i < msdescs.size(); i++) {
            if (msdescs[i].UDN.compare(m_msdescs[i].UDN)) {
                same = false;
                break;
            }
        }
    }
    if (!same) {
        LOGDEB("CDBrowser::initialPage: servers list changed\n");
        m_msdescs = msdescs;
    }
    // If the list did not change and a redisplay was not requested,
    // do nothing
    if (same && !redisplay && m_initUDN.isEmpty()) {
        LOGDEB1("CDBrowser::initialPage: not redisplaying\n");
        m_timer.start(1000);
        return;
    }

    // Displaying stuff

    if (!m_initUDN.isEmpty() && m_curpath.size() > 1) {
        LOGDEB("CDBrowser::initialPage: auto-navigate to server\n");
        // Called from browseinnewtab, with specified server and path
        // (either midclick or restoring from stored state): show
        // appropriate container
        int i = serverFromUDN(qs2utf8s(m_initUDN));
        //qDebug() << "initialPage: serverFromUDN returned " << i;
        if (i >= 0) {
            if (newCds(i)) {
                m_initUDN = "";
                curpathClicked(m_curpath.size() - 1);
                return;
            }
        }
    }

    // No initUDN or not found: show servers list
    mySetHtml(emptyServersPage());
    for (unsigned i = 0; i < msdescs.size(); i++) {
        // Alphamap does not really work for servers, which are not in
        // alphabetic order at the moment
        // updateAlphamap(curinitial, msdescs[i].friendlyName);
        appendHtml("", DSToHtml(i, msdescs[i]));
    }
    m_timer.start(1000);
}

enum PopupMode {
    PUP_ADD,
    PUP_ADD_ALL,
    PUP_ADD_FROMHERE,
    PUP_BACK,
    PUP_OPEN_IN_NEW_TAB,
    PUP_RAND_PLAY_TRACKS,
    PUP_RAND_PLAY_GROUPS,
    PUP_RAND_STOP,
    PUP_SORT_ORDER,
    PUP_COPY_URL,
    PUP_AS_CSV,
};

#ifdef USING_WEBENGINE

#if (QT_VERSION < QT_VERSION_CHECK(5, 14, 0))
#define QTSKIPEMPTYPARTS QString::SkipEmptyParts
#else
#define QTSKIPEMPTYPARTS Qt::SkipEmptyParts
#endif

void CDBrowser::onPopupJsDone(const QVariant &jr)
{
    QString qs(jr.toString());
    LOGDEB("onPopupJsDone: parameter: " << qs2utf8s(qs) << endl);
    QStringList qsl = qs.split("\n", QTSKIPEMPTYPARTS);
    for (int i = 0 ; i < qsl.size(); i++) {
        int eq = qsl[i].indexOf("=");
        if (eq > 0) {
            QString nm = qsl[i].left(eq).trimmed();
            QString value = qsl[i].right(qsl[i].size() - (eq+1)).trimmed();
            if (!nm.compare("objid")) {
                m_popupobjid = qs2utf8s(value);
            } else if (!nm.compare("title")) {
                m_popupobjtitle = qs2utf8s(value);
            } else if (!nm.compare("objidx")) {
                m_popupidx = value.toInt();
            } else if (!nm.compare("otype")) {
                m_popupotype = qs2utf8s(value);
            } else if (!nm.compare("ispl")) {
                m_popupispl = value.toInt();
            } else {
                LOGERR("onPopupJsDone: unknown key: " << qs2utf8s(nm) << endl);
            }
        }
    }
    LOGDEB1("onPopupJsDone: class [" << m_popupotype <<
           "] objid [" << m_popupobjid <<
           "] title [" <<  m_popupobjtitle <<
           "] objidx [" << m_popupidx << "]\n");
    doCreatePopupMenu();
}
#endif

void CDBrowser::createPopupMenu(const QPoint& pos)
{
    if (!m_browsers || m_browsers->insertActive()) {
        LOGDEB("CDBrowser::createPopupMenu: no popup: insert active\n");
        return;
    }
    LOGDEB("CDBrowser::createPopupMenu\n");
    m_popupobjid = m_popupobjtitle = m_popupotype = "";
    m_popupidx = -1;
    m_popupos = pos;
    
#ifdef USING_WEBENGINE
    QString js("window.locDetails;");
    CDWebPage *mypage = dynamic_cast<CDWebPage*>(page());
    mypage->runJavaScript(js, [this](const QVariant &v) { onPopupJsDone(v); });
#else
    QWebHitTestResult htr = page()->mainFrame()->hitTestContent(pos);
    if (htr.isNull()) {
        LOGDEB("CDBrowser::createPopupMenu: no popup: no hit\n");
        return;
    }
    QWebElement el = htr.enclosingBlockElement();
    while (!el.isNull() && !el.hasAttribute("objid"))
        el = el.parent();
    if (!el.isNull()) {
        m_popupobjid = qs2utf8s(el.attribute("objid"));
        m_popupobjtitle = qs2utf8s(el.toPlainText());
        if (el.hasAttribute("objidx"))
            m_popupidx = el.attribute("objidx").toInt();
        else
            m_popupidx = -1;
        if (el.hasAttribute("ispl"))
            m_popupispl = el.attribute("ispl").toInt();
        else
            m_popupispl = false;
        m_popupotype = qs2utf8s(el.attribute("class"));
        LOGDEB("Popup: " << " class " << m_popupotype << " objid " << 
               m_popupobjid << endl);
    }
    doCreatePopupMenu();
#endif
}

void CDBrowser::popupaddaction(QMenu *popup, const QString& txt, int value)
{
    QAction *act = new QAction(txt, this);
    act->setData(value);
    popup->addAction(act);
}

void CDBrowser::doCreatePopupMenu()
{
    LOGDEB1("CDBrowser::doCreatePopupMenu: objid " << m_popupobjid <<
           " title " << m_popupobjtitle << " type " <<  m_popupotype <<
           " IDX " << m_popupidx << " ISPL " << m_popupispl << endl);

    if (m_curpath.size() == 0) {
        // No popup on servers page at the moment.
        return;
    }

    QMenu *popup = new QMenu(this);

    // Click in blank area, or no playlist
    if (m_popupobjid.empty() || (!m_browsers || !m_browsers->have_playlist())) {
        popupaddaction(popup, tr("Back"), PUP_BACK);
        if (m_browsers->randPlayActive()) {
            popupaddaction(popup, tr("Stop random play"), PUP_RAND_STOP);
        }
        popupaddaction(popup, tr("Save as CSV"), PUP_AS_CSV);
        popup->connect(popup, SIGNAL(triggered(QAction *)),
                       this, SLOT(popupOther(QAction *)));
        popup->popup(mapToGlobal(m_popupos));
        return;
    }

    if (m_popupidx == -1 && m_popupotype.compare("container")) {
        // Click in curpath. All path elements should be containers !
        LOGERR("doCreatePopupMenu: not container and no objidx??\n");
        return;
    }

    // Either item or container can be sent to playlist. Different
    // kind of add though, see below
    popupaddaction(popup, tr("Send to playlist"), PUP_ADD);

    if (!m_popupotype.compare("item")) {
        popupaddaction(popup, tr("Send all to playlist"), PUP_ADD_ALL);
        popupaddaction(popup, tr("Send all from here to playlist"),
                       PUP_ADD_FROMHERE);
        popupaddaction(popup, tr("Copy URL"), PUP_COPY_URL);
        popup->connect(popup, SIGNAL(triggered(QAction *)), this, 
                       SLOT(simpleAdd(QAction *)));
    } else if (!m_popupotype.compare("container")) {
        popupaddaction(popup, tr("Open in new tab"), PUP_OPEN_IN_NEW_TAB);
        popupaddaction(popup, tr("Random play by tracks"), PUP_RAND_PLAY_TRACKS);
        popupaddaction(popup, tr("Random play by groups"), PUP_RAND_PLAY_GROUPS);
        popup->connect(popup, SIGNAL(triggered(QAction *)), this, 
                       SLOT(recursiveAdd(QAction *)));
    } else {
        // ??
        LOGDEB("CDBrowser::createPopup: obj type neither ct nor it: " <<
               m_popupotype << endl);
        return;
    }

    if (m_browsers->randPlayActive()) {
        popupaddaction(popup, tr("Stop random play"), PUP_RAND_STOP);
    }
    popupaddaction(popup, tr("Sort order"), PUP_SORT_ORDER);
    popupaddaction(popup, tr("Save as CSV"), PUP_AS_CSV);

    popup->popup(mapToGlobal(m_popupos));
}

void CDBrowser::back(QAction *)
{
    if (m_curpath.size() >= 2)
        curpathClicked(m_curpath.size() - 2);
}

bool CDBrowser::popupOther(QAction *act)
{
    m_popupmode = act->data().toInt();
    switch (m_popupmode) {
    case PUP_BACK:
        back(0);
        break;
    case PUP_AS_CSV:
        saveAsCSV();
        break;
    case PUP_RAND_STOP:
        emit sig_rand_stop();
        break;
    case PUP_SORT_ORDER:
        emit sig_sort_order();
        break;
    default:
        return false;
    }
    return true;
}

// Add a single track or a section of the current container. This
// maybe triggered by a link click or a popup on an item entry
void CDBrowser::simpleAdd(QAction *act)
{
    //qDebug() << "CDBrowser::simpleAdd";
    if (popupOther(act)) {
        // Not for us
        return;
    }
    if (m_popupidx < 0 || m_popupidx > int(m_entries.size())) {
        LOGERR("CDBrowser::simpleAdd: bad obj index: " << m_popupidx
               << " id count: " << m_entries.size() << endl);
        return;
    }
    MetaDataList mdl;
    unsigned int starti = 0;
    switch (m_popupmode) {
    case PUP_COPY_URL:
    {
        UPnPDirObject& e(m_entries[m_popupidx]);
        if (e.m_resources.empty()) {
            
        }
        QString uri = QString::fromLocal8Bit(e.m_resources[0].m_uri.c_str());
        QApplication::clipboard()->setText(uri, QClipboard::Selection);
        QApplication::clipboard()->setText(uri, QClipboard::Clipboard);
        return;
    }
    case PUP_ADD_FROMHERE: 
        starti = m_popupidx;
        /* FALLTHROUGH */
    case PUP_ADD_ALL:
    {
        bool allok{true};
        for (unsigned int i = starti; i < m_entries.size(); i++) {
            if (m_entries[i].m_type == UPnPDirObject::item && 
                m_entries[i].m_iclass == 
                UPnPDirObject::ITC_audioItem_musicTrack) {
                mdl.resize(mdl.size()+1);
                if (!udirentToMetadata(&m_entries[i], &mdl[mdl.size()-1],
                                       m_browsers->get_pl())) {
                    allok = false;
                }
            }
        }
        if (!allok) {
            QMessageBox::warning(0, "Upplay",
                                 "Some tracks could not be loaded: "
                                 "no compatible audio format");
        }
    }        
        break;
    default:
        mdl.resize(1);
        if (!udirentToMetadata(&m_entries[m_popupidx], &mdl[0],
                               m_browsers->get_pl())) {
            QMessageBox::warning(0, "Upplay", "No compatible audio format");
        }
    }

    emit sig_tracks_to_playlist(mdl);
}

void CDBrowser::saveAsCSV()
{
    string csv;
    for (const auto& e : m_entries) {
        vector<string> fields;
        string artist;
        e.getprop("upnp:artist", artist);
        if (e.m_type == UPnPDirObject::container) {
            fields = vector<string>{"CT", "", e.m_title, artist, "", ""};
        } else {
            string tno;
            e.getprop("upnp:originalTrackNumber", tno);
            string alb;
            e.getprop("upnp:album", alb);
            string sdur;
            e.getrprop(0, "duration", sdur);
            int seconds = upnpdurationtos(sdur);
            fields = vector<string>{"IT", tno, e.m_title, artist, alb,
                                    lltodecstr(seconds)};
        }
        string s;
        stringsToCSV(fields, s);
        csv += s + "\n";
    }
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"));
    if (!fileName.isEmpty()) {
        string fn = qs2utf8s(fileName);
        int fd;
        if ((fd = ::open(fn.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0644)) < 0) {
            QMessageBox::warning(0, "Upplay", "can't open/create");
            return;
        }
        int ret = ::write(fd, csv.c_str(), csv.size());
        ((void)ret);
        ::close(fd);
    }
}

string CDBrowser::getServerUDN()
{
    if (m_cds) {
        return m_cds->srv()->getDeviceId();
    }
    return string();
}

// Recursive add. This is triggered popup on a container
void CDBrowser::recursiveAdd(QAction *act)
{
    //qDebug() << "CDBrowser::recursiveAdd";

    deleteReaders("recursiveAdd");
        
    if (popupOther(act)) {
        // Not for us
        return;
    }

    if (!m_cds) {
        LOGERR("CDBrowser::recursiveAdd: server not set\n") ;
        return;
    }

    if (m_popupmode == PUP_OPEN_IN_NEW_TAB) {
        vector<CtPathElt> npath(m_curpath);
        npath.push_back(CtPathElt(m_popupobjid, m_popupobjtitle, m_popupispl));
        emit sig_browse_in_new_tab(u8s2qs(m_cds->srv()->getDeviceId()), npath);
        return;
    }

    updateFormatParams();
    
    if (m_browsers)
        m_browsers->setInsertActive(true);
    ContentDirectory::ServiceKind kind = m_cds->srv()->getKind();
    if (kind == ContentDirectory::CDSKIND_MINIM &&
        m_popupmode != PUP_RAND_PLAY_GROUPS) {
        // Use search() rather than a tree walk for Minim, it is much
        // more efficient, except for rand play albums, where we want
        // to preserve the paths (for discrimination)
        UPnPDirContent dirbuf;
        string ss("upnp:class = \"object.item.audioItem.musicTrack\"");
        int err = m_cds->srv()->search(m_popupobjid, ss, dirbuf);
        if (err != 0) {
            LOGERR("CDBrowser::recursiveAdd: search failed, code " << err);
            return;
        }

        m_recwalkentries = dirbuf.m_items;

        // If we are inside the folders view tree, and the option is
        // set, sort by url (minim sorts by tracknum tag even inside
        // folder view)
        if (CSettingsStorage::getInstance()->getSortKind() == 
            CSettingsStorage::SK_MINIMFNORDER && 
            m_popupobjid.compare(0, minimFoldersViewPrefix.size(),
                                 minimFoldersViewPrefix) == 0) {
            vector<string> crits;
            crits.push_back("uri");
            DirObjCmp cmpo(crits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }

        rreaperDone(0);

    } else {
        delete m_progressD;
        m_progressD = new QProgressDialog("Exploring Directory          ",
                                          tr("Cancel"), 0, 0, this);
        // can't get setMinimumDuration to work
        m_progressD->setMinimumDuration(2000);
        time(&m_progstart);
        
        m_recwalkentries.clear();
        m_recwalkdedup.clear();
        m_reaper = new RecursiveReaper(m_cds->srv(), m_popupobjid, this);
        connect(m_reaper, 
                SIGNAL(sliceAvailable(UPnPClient::UPnPDirContent *)),
                this, 
                SLOT(onReaperSliceAvailable(UPnPClient::UPnPDirContent *)));
        connect(m_reaper, SIGNAL(done(int)), this, SLOT(rreaperDone(int)));
        m_reaper->start();
    }
}

void CDBrowser::onReaperSliceAvailable(UPnPClient::UPnPDirContent *dc)
{
    LOGDEB("CDBrowser::onReaperSliceAvailable: got " << 
           dc->m_items.size() << " items\n");
    if (!m_reaper) {
        LOGINF("CDBrowser::onReaperSliceAvailable: cancelled\n");
        delete dc;
        return;
    }
    for (unsigned int i = 0; i < dc->m_items.size(); i++) {
        if (dc->m_items[i].m_resources.empty()) {
            LOGDEB("CDBrowser::onReaperSlice: entry has no resources??"<< endl);
            continue;
        }
        //LOGDEB1("CDBrowser::onReaperSlice: uri: " << 
        //       dc->m_items[i].m_resources[0].m_uri << endl);
        string md5;
        MD5String(dc->m_items[i].m_resources[0].m_uri, md5);
        pair<std::unordered_set<std::string>::iterator, bool> res = 
            m_recwalkdedup.insert(md5);
        if (res.second) {
            m_recwalkentries.push_back(dc->m_items[i]);
        } else {
            LOGDEB("CDBrowser::onReaperSlice: dup found" << endl);
        }
    }
    delete dc;
    if (m_progressD) {
        if (m_progressD->wasCanceled()) {
            delete m_progressD;
            m_progressD = 0;
            m_reaper->setCancel();
        } else {
            if (time(0) - m_progstart >= 1) {
                m_progressD->show();
            }
            m_progressD->setLabelText(
                tr("Exploring Directory: %1 tracks").
                arg(m_recwalkentries.size()));
        }
    }
}

void CDBrowser::rreaperDone(int status)
{
    LOGDEB("CDBrowser::rreaperDone: status: " << status << ". Entries: " <<
           m_recwalkentries.size() << endl);
    deleteReaders("rreaperDone");
    delete m_progressD;
    m_progressD = 0;
    if (m_popupmode == PUP_RAND_PLAY_TRACKS ||
        m_popupmode == PUP_RAND_PLAY_GROUPS) {
        if (m_browsers)
            m_browsers->setInsertActive(false);
        // Sort list
        if (m_popupmode == PUP_RAND_PLAY_GROUPS) {
            vector<string> sortcrits;
            sortcrits.push_back("upplay:ctpath");
            sortcrits.push_back("upnp:album");
            sortcrits.push_back("upnp:originalTrackNumber");
            DirObjCmp cmpo(sortcrits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }
        RandPlayer::PlayMode mode = m_popupmode == PUP_RAND_PLAY_TRACKS ?
            RandPlayer::PM_TRACKS : RandPlayer::PM_GROUPS;
        LOGDEB("CDBrowser::rreaperDone: emitting sig_tracks_to_randplay, mode "
               << mode << endl);
        emit sig_tracks_to_randplay(mode, m_recwalkentries);
    } else {
        int sortkind = CSettingsStorage::getInstance()->getSortKind();
        if (sortkind == CSettingsStorage::SK_CUSTOM) {
            QStringList qcrits =
                CSettingsStorage::getInstance()->getSortCrits();
            vector<string> sortcrits;
            for (int i = 0; i < qcrits.size(); i++) {
                sortcrits.push_back(qs2utf8s(qcrits[i]));
            }
            DirObjCmp cmpo(sortcrits);
            sort(m_recwalkentries.begin(), m_recwalkentries.end(), cmpo);
        }
        MetaDataList mdl;
        mdl.resize(m_recwalkentries.size());
        bool allok{true};
        for (unsigned int i = 0; i <  m_recwalkentries.size(); i++) {
            if (!udirentToMetadata(&m_recwalkentries[i], &mdl[i],
                                   m_browsers->get_pl())) {
                allok = false;
            }
        }
        if (!allok) {
            QMessageBox::warning(0, "Upplay",
                                 "Some tracks could not be loaded: "
                                 "no compatible audio format");
        }
        emit sig_tracks_to_playlist(mdl);
    }
}
