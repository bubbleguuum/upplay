/* Copyright (C) 2011  Lucio Carreras
 * Copyright (C) 2017 J.F. Dockes
 *
 * This file is part of Upplay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STYLE_H_
#define STYLE_H_

#include <QString>
#include <string>

namespace Style {

/** Retrieve the application qss style sheet, read from disk, adjusted
 * for color style and scale */
QString get_style(bool dark, float multiplier = 1.0);

/** Adjust font-size lines in input CSS/QSS string according to
   multiplier. Only touches fonts given in 'pt' units, and the
   directives must each happen in their own line. */
std::string scale_fonts(const std::string& style, float multiplier);

};


#endif /* STYLE_H_ */
