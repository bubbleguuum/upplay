/* Copyright (C) 2014-2018 J.F.Dockes
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef _AVTRANSPORT_QO_INCLUDED
#define _AVTRANSPORT_QO_INCLUDED

#include <string>

#include <QObject>
#include <QThread>
#include <QTimer>

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/control/avtransport.hxx"
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
#include "libupnpp/control/conman.hxx"
#endif
#include "libupnpp/control/cdircontent.hxx"
#include "libupnpp/log.h"
#include "smallut.h"

using namespace UPnPClient;

class AVTMetadata {
public:
    virtual std::string getDidl() const = 0;
};

class AVTransportQO : public QObject, public UPnPClient::VarEventReporter {
    Q_OBJECT;

public:
    AVTransportQO(UPnPClient::AVTH avt,
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
                  UPnPClient::CNMH conman,
#endif
                  QObject *parent = 0)
        : QObject(parent),
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
          m_conman(conman),
#endif
          m_srv(avt) {

        m_timer = new QTimer(this);
        connect(m_timer, SIGNAL(timeout()), this, SLOT(update()));
        m_timer->start(1000);

        qRegisterMetaType<UPnPClient::UPnPDirObject>
            ("UPnPClient::UPnPDirObject");

        m_srv->installReporter(this);

        // We are handling the playlist: set the renderer in "normal"
        // mode. Actually, I don't think that this is relevant at all,
        // we never consider that the renderer may have an internal
        // playlist.
        m_srv->setPlayMode(UPnPClient::AVTransport::PM_Normal);
        if (!stringicmp(m_srv->getModelName(), "gmediarender")) {
            m_stop_before_seturi = true;
        }
    }

    virtual ~AVTransportQO() {
        m_srv->installReporter(0);
    }

    const char *tpstatetostr(int tps) {
        switch (tps) {
        default:
        case UPnPClient::AVTransport::Unknown: return "Unknown";
        case UPnPClient::AVTransport::Stopped: return "Stopped";
        case UPnPClient::AVTransport::Playing: return "Playing";
        case UPnPClient::AVTransport::Transitioning: return "Transitionning";
        case UPnPClient::AVTransport::PausedPlayback: return "PausedPlay";
        case UPnPClient::AVTransport::PausedRecording: return "PausedRecord";
        case UPnPClient::AVTransport::Recording: return "Recording";
        case UPnPClient::AVTransport::NoMediaPresent: return "No Media";
        }
    }

    virtual void changed(const char *nm, int value) {
        if (!strcmp(nm, "CurrentTrackDuration")) {
            // This is normally part of LastChange? but some renderers
            // apparently don't send it (bubble?). So use the value
            // from GetPositionInfo
            LOGDEB1("AVT: Changed: " << nm << " (int): " << value << "\n");
            m_cursecs = value;
        } else if (!strcmp(nm, "TransportState")) {
            LOGDEB("AVT: Changed: " << nm << " " << tpstatetostr(value) << "\n");
            m_tpstate = AVTransport::TransportState(value);
            emit tpStateChanged(value);
            if (m_in_ending &&
                (value == UPnPClient::AVTransport::Stopped ||
                 value == UPnPClient::AVTransport::NoMediaPresent)) {
                m_in_ending = false;
                LOGDEB("AVT: changed: emitting stoppedAtEOT\n");
                emit stoppedAtEOT();
            }
        } else if (!strcmp(nm, "CurrentTransportActions")) {
            LOGDEB1("AVT: changed: " << nm << " (int): " << value << "\n");
            emit tpActionsChanged(value);
        }
    }

    // Note about uri change detection:
    // We use a changed uri to reset the state for sending out a setNextURI
    //
    // This fails if 2 identical URIs are consecutive in the track
    // list. The consequence in this situation is that, if there is a
    // 3rd track and it should be gapless with the second, this won't
    // happen, there will be a stop/start. This does not seem like a
    // big deal.
    //
    // This could be mitigated by adding a time based track-change
    // detection (there is infortunately no sequential "tracks played"
    // counter in UPnP). If the time goes back more than a few seconds
    // without a command from us, then we have a track change.
    virtual void changed(const char *nm, const char *value) {
        LOGDEB1("AVT: Changed: " << nm << " (char*): " << value << "\n");
        if (!strcmp(nm, "AVTransportURI")) {
            if (m_cururi.compare(value) &&
                (m_tpstate == UPnPClient::AVTransport::Playing ||
                 m_tpstate == UPnPClient::AVTransport::Transitioning ||
                 m_tpstate == UPnPClient::AVTransport::PausedPlayback)) {
                LOGDEB("AVT: ext track change: cur [" << m_cururi <<
                       "] new [" << value << "]\n");
                setcururi(value);
                emit newTrackPlaying(QString::fromUtf8(value));
            }
        }
    }

    virtual void changed(const char *nm, UPnPClient::UPnPDirObject meta) {
        if (!strcmp(nm, "AVTransportURIMetaData")) {
            LOGDEB1("AVT: Changed: "<<nm << " (dirc): " << meta.dump() << "\n");
            // Don't use this if no resources are set. XBMC/Kodi does
            // this for some reason. Else we'd end-up with
            // resource-less unplayable entries in the
            // playlist. Scheduling a state update is not useful
            // either because the data will have the same
            // problem. Kodi only emits useful metadata when
            // explicitely told to switch tracks (not even at regular
            // track changes).
            if (!meta.m_resources.empty() &&
                (m_tpstate == UPnPClient::AVTransport::Playing ||
                 m_tpstate == UPnPClient::AVTransport::PausedPlayback)) {
                emit currentMetadata(meta);
            }
        }
    }

public slots:

    virtual void play() {
        LOGDEB("AVT: play. m_pauseforunpause: " << m_pauseforunpause <<
               " m_tpstate : " << m_tpstate << "\n");
        if (m_pauseforunpause &&
            m_tpstate == UPnPClient::AVTransport::PausedPlayback) {
            // Bug in some Naim streamer: pause as toggle, contrary to upnp spec.
            m_srv->pause();
        } else {
            m_srv->play();
        }
    }
    virtual void stop() {
        LOGDEB("AVT: stop\n");
        setcururi("");
        m_in_ending = false;
        m_srv->stop();
    }
    virtual void pause() {
        LOGDEB("AVT: pause\n");
        m_srv->pause();
    }

    virtual void changeTrack(const std::string& uri, const AVTMetadata* md) {
        LOGDEB("AVT: changeTrack: " << uri << "\n");
        if (m_stop_before_seturi) {
            m_srv->stop();
        }
        m_srv->setAVTransportURI(uri, md->getDidl());
        // Don't do this: wait for the renderer data, else we risk
        // flickering if an event reports the old track.
        // setcururi(uri);
    }

    virtual void prepareNextTrack(const std::string& uri,
                                  const AVTMetadata* md) {
        LOGDEB("AVT: prepareNextTrack: " << uri << "\n");
        m_srv->setNextAVTransportURI(uri, md->getDidl());
    }

    // Seek to point. Parameter in seconds
    virtual void seek(int secs) {
        LOGDEB("AVT: seek to " << secs << " S. m_cursecs " << m_cursecs <<"\n");
        m_srv->seek(UPnPClient::AVTransport::SEEK_REL_TIME, secs);
    }

    // Retrieve the current track length in seconds. This is useful if
    // the available metadata does not have a duration (esp. happens
    // with video)
    virtual int trackSecs() {
        return m_cursecs;
    }
    virtual void fetchState() {
        update(true);
    }

    virtual bool checkConnection() {
        UPnPClient::AVTransport::PositionInfo info;
        for (int i = 0; i < 2; i++) {
            int error;
            if ((error = m_srv->getPositionInfo(info)) != 0) {
                LOGINF("AVT: getPositionInfo failed with error "<<error<<"\n");
            } else {
                return true;
            }
        }
        return false;
    }

    // Called by timer every sec
    virtual void update(bool force = false) {

#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
        if (m_conman) {
            std::vector<UPnPP::ProtocolinfoEntry> srcinfo;
            m_conman->getProtocolInfo(srcinfo, m_protoinfo);
            sortprotoinfo();
        }
#endif
        
        UPnPClient::AVTransport::PositionInfo info;
        int error;
        if ((error = m_srv->getPositionInfo(info)) != 0) {
            LOGINF("AVT: getPositionInfo failed with error " << error << "\n");
            if (m_errcnt++ > 4) {
                emit connectionLost();
            }
            return;
        }
        m_errcnt = 0;
        m_cursecs = info.trackduration;

        UPnPClient::AVTransport::TransportInfo tinfo;
        if ((error = m_srv->getTransportInfo(tinfo)) != 0) {
            LOGINF("AVT: getTransportInfo failed with error " << error << "\n");
            return;
        }

        LOGDEB0("AVT: update: posinfo: reltime " << info.reltime <<
                " tdur " << info.trackduration << " meta " <<
                info.trackmeta.dump() << "\n");

        if (m_tpstate == UPnPClient::AVTransport::Playing) {
            // Time-related stuff
            emit secsInSongChanged(info.reltime);
            if (m_cursecs > 0) {
                if (info.reltime > m_cursecs - 10) {
                    m_in_ending = true;
                } else if (info.reltime > 0 && info.reltime < 5) {
                    // This is for the case where we are playing 2
                    // consecutive identical URIs: heuristic try to detect
                    // the change
                    if (m_in_ending == true) {
                        LOGDEB("AVT: was in end, seeing start: trkswitch\n");
                        setcururi(info.trackuri);
                        emit newTrackPlaying(u8s2qs(info.trackuri));
                    }
                }
            } else if (info.reltime > 0) {
                // heuristic attempt to support really crappy
                // AVMFritz!MediaRenderer which sends incorrect XML in
                // LastChange (unescaped didl), and 0 for track
                // duration. To force upplay to switch to the next
                // track when the player stops.
                m_in_ending = true;
            }
        }
        
        if (force || tinfo.tpstate != m_tpstate) {
            LOGDEB("AVT: emitting tpStateChanged:" <<
                   tpstatetostr(tinfo.tpstate) << "\n");
            emit tpStateChanged(tinfo.tpstate);
            m_tpstate = tinfo.tpstate;
        }
        if (force || (m_cururi.compare(info.trackuri) &&
                      (m_tpstate == UPnPClient::AVTransport::Playing ||
                       m_tpstate == UPnPClient::AVTransport::Transitioning ||
                       m_tpstate == UPnPClient::AVTransport::PausedPlayback))) {
            LOGDEB("AVT: update: ext track change: cur [" <<
                   m_cururi << "] new [" << info.trackuri << "]\n");
            setcururi(info.trackuri);
            if (!info.trackmeta.m_resources.empty()) {
                // Don't emit bogus meta which would unplayable
                // entries in the playlist. The only moment when Kodi
                // will emit usable metadata is when told to change
                // tracks. MediaInfo returns the same data.
                LOGDEB("AVT: emitting currentMetadata\n");
                emit currentMetadata(info.trackmeta);
            }
            LOGDEB("AVT: emitting newTrackPlaying: " << info.trackuri << "\n");
            emit newTrackPlaying(u8s2qs(info.trackuri));
        }
        if (m_in_ending &&
            (tinfo.tpstate == UPnPClient::AVTransport::Stopped ||
             tinfo.tpstate == UPnPClient::AVTransport::NoMediaPresent)) {
            m_in_ending = false;
            LOGDEB("AVT: emitting stoppedAtEOT\n");
            emit stoppedAtEOT();
        }
    }

signals:
    void secsInSongChanged(quint32);
    void newTrackPlaying(QString);
    void tpStateChanged(int);
    void tpActionsChanged(int);
    void stoppedAtEOT();
    void currentMetadata(UPnPClient::UPnPDirObject);
    void connectionLost();

protected:
    // Quirks
    bool m_pauseforunpause{false};
#ifdef LIBUPNPP_HAS_UPNPAVCONMAN
    std::vector<UPnPP::ProtocolinfoEntry> m_protoinfo;
    UPnPClient::CNMH m_conman;
    void sortprotoinfo() {
        std::sort(m_protoinfo.begin(), m_protoinfo.end(),
                  [] (const UPnPP::ProtocolinfoEntry&a,
                      const UPnPP::ProtocolinfoEntry&b)->bool {
                      return a.contentFormat < b.contentFormat;
                  });
    }
#endif
    
private:
    UPnPClient::AVTH m_srv;
    QTimer *m_timer{nullptr};
    int m_errcnt{0};
    int m_cursecs{-1};
    bool m_in_ending{false};
    // Some renderers need this (e.g. gmediarender). We'd probably
    // need to have some kind of quirks database somewhere instead of
    // testing left and right
    bool m_stop_before_seturi{false};
    std::string m_cururi;
    AVTransport::TransportState m_tpstate{AVTransport::Unknown};

    void setcururi(const std::string& uri) {
        LOGDEB("AVT: setcururi: " << uri << "\n");
        m_cururi = uri;
        if (uri != "") {
            // Don't reset m_in_ending if uri is null: we often get an
            // uri change to "" before we get the transport state
            // change event. Resetting m_in_ending would prevent the
            // emission of the stoppedAtEOT signal
            m_in_ending = false;
        }
    }
    QString u8s2qs(const std::string us) {
        return QString::fromUtf8(us.c_str());
    }

};

#endif // _AVTRANSPORT_QO_INCLUDED
