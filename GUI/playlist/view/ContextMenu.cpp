/* Copyright (C) 2013  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ContextMenu.h"
#include "HelperStructs/Helper.h"

#include <QAction>
#include <QList>
#include <QDebug>

ContextMenu::ContextMenu(QWidget* parent)
    : QMenu(parent)
{
    _remove_action = new QAction(QIcon(Helper::getIconPath("delete.png")),
                                 tr("Remove"), this);
    _sort_tno_action = new QAction(tr("Sort by track number"), this);
    _invert_selection_action = new QAction(tr("Invert Selection"), this);
    _clear_selection_action = new QAction(tr("Clear Selection"), this);
    _select_all_action = new QAction(tr("Select All"), this);
}

void ContextMenu::setup_entries(int entries)
{
    QList<QAction*> actions = this->actions();
    foreach(QAction *a, actions) {
        this->removeAction(a);
        a->disconnect();
    }

    if (entries & ENTRY_REMOVE) {
        this->addAction(_remove_action);
        connect(_remove_action, SIGNAL(triggered()),
                this, SIGNAL(sig_remove_clicked()));
    }
    if (entries & ENTRY_SORT_TNO) {
        this->addAction(_sort_tno_action);
        connect(_sort_tno_action, SIGNAL(triggered()),
                this, SIGNAL(sig_sort_tno_clicked()));
    }
    if (entries & ENTRY_INVERT_SELECTION) {
        this->addAction(_invert_selection_action);
        connect(_invert_selection_action, SIGNAL(triggered()),
                this, SIGNAL(sig_invert_selection_clicked()));
    }
    if (entries & ENTRY_CLEAR_SELECTION) {
        this->addAction(_clear_selection_action);
        connect(_clear_selection_action, SIGNAL(triggered()),
                this, SIGNAL(sig_clear_selection_clicked()));
    }
    if (entries & ENTRY_SELECT_ALL) {
        this->addAction(_select_all_action);
        connect(_select_all_action, SIGNAL(triggered()),
                this, SIGNAL(sig_select_all_clicked()));
    }
}
