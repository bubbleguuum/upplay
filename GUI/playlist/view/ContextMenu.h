/* Copyright (C) 2013  Lucio Carreras
 *
 * This file is part of sayonara player
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CONTEXTMENU_H
#define CONTEXTMENU_H

#include <QMenu>
#include <QAction>
#include <QEvent>

enum PopupEntry {
    ENTRY_NONE, ENTRY_INFO = 0x1, ENTRY_REMOVE=0x2, ENTRY_SORT_TNO = 0x4,
    ENTRY_INVERT_SELECTION = 0x8, ENTRY_CLEAR_SELECTION = 0x10,
    ENTRY_SELECT_ALL = 0x20};

class ContextMenu : public QMenu {
    Q_OBJECT

public:
    explicit ContextMenu(QWidget *parent = 0);
    void setup_entries(int entries);

signals:
    void sig_remove_clicked();
    void sig_sort_tno_clicked();
    void sig_invert_selection_clicked();
    void sig_clear_selection_clicked();
    void sig_select_all_clicked();

private:
    QAction*            _remove_action;
    QAction*            _sort_tno_action;
    QAction*            _invert_selection_action;
    QAction*            _clear_selection_action;
    QAction*            _select_all_action;
};

#endif // CONTEXTMENU_H
