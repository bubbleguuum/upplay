/* Copyright (C) 2017-2019 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef _RENDERERLIST_H_INCLUDED_
#define _RENDERERLIST_H_INCLUDED_

#include <vector>
#include <libupnpp/control/description.hxx>
#include <libupnpp/control/mediarenderer.hxx>

enum RendererSelectFlags{RSF_NONE = 0, RSF_OHONLY = 1, RSF_AVTONLY = 2};
extern std::vector<UPnPClient::UPnPDeviceDesc> rendererList(
    int flags = RSF_NONE);

extern UPnPClient::MRDH getRenderer(const std::string& name, bool isfriendlynm);

#endif /* _RENDERERLIST_H_INCLUDED_ */
